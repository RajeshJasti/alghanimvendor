﻿var selected = [];

//Select Business Unit(IT, Retail) radio button and click 'Proceed' button
$('#selectedBU_Next').click(function () {
    $('#selectBU_RadioBoxes input:checked').each(function () {
        selected.push($(this).parent().find('label').text());
    });

    $('#Select_BU').css('display', 'none');
    $('#select_Vendor').css('display', 'block');
});

//Click 'New Vendor' image
$('#newVendor').click(function () {
    if (selected[0] === 'Information Technology') {
        $('#newVendor').attr('href', 'https://vendorreg.azurewebsites.net/it/reg.aspx');
    }
    else {
        $('#newVendor').attr('href', location.origin + "/VendorCreate/Index");
    }
});

//Click 'Existing Vendor' image
$('#existingVendor').click(function () {
    if (selected[0] === 'Information Technology') {
        $('#existingVendor').attr('href', 'https://vendorreg.azurewebsites.net/it/reg.aspx');
    }
    else {
        $('#existingVendor').attr('href', location.origin + "/OTP/GenerateOTP");
    }
});

//Click 'Back' button below 'New Vendor' and 'Existing Vendor' image
$('#vendorBackBtn').click(function () {
    alert('asde456');
    $('#Select_BU').css('display', 'block');
    $('#select_Vendor').css('display', 'none');
});