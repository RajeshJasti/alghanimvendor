﻿var selected = [];
var RemoteURL = "http://localhost/RetailVendor";

//Select Business Unit(IT, Retail) radio button and click 'Proceed' button
$('#selectedBU_Next').click(function () {
    $('#selectBU_RadioBoxes input:checked').each(function () {
        selected.push($(this).parent().find('label').text());
    });

    $('#Select_BU').css('display', 'none');
    $('#select_Vendor').css('display', 'block');
});

//Click 'New Vendor' image
$('#newVendor').click(function () {
    if (selected[0] === 'Information Technology') {
        $('#newVendor').attr('href', 'https://vendorreg.azurewebsites.net/it/reg.aspx');
    }
    else {
        //For Local
        //$('#newVendor').attr('href', location.origin + "/VendorCreate/Index");

        //For Remote
        $('#newVendor').attr('href', RemoteURL + "/VendorCreate/Index");
    }
});

//Click 'Existing Vendor' image
$('#existingVendor').click(function () {
    if (selected[0] === 'Information Technology') {
        $('#existingVendor').attr('href', 'https://vendorreg.azurewebsites.net/it/reg.aspx');
    }
    else {
        // For Local
        //$('#existingVendor').attr('href', location.origin + "/OTP/GenerateOTP");

        // For Remote
        $('#existingVendor').attr('href', RemoteURL + "/OTP/GenerateOTP");
    }
});

//Click 'Back' button below 'New Vendor' and 'Existing Vendor' image
$('#vendorBackBtn').click(function () {
    $('#Select_BU').css('display', 'block');
    $('#select_Vendor').css('display', 'none');
});