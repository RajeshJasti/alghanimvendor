﻿$(function () {
    var update = null;
    var finalOutput = [
        {
            "Company_Info": {
                "CompanyCode": 0,
                "PurchaseOrg": ""
            },
            "Address_Info1": {
                "Address1_Title": "",
                "Address1_Name1": "",
                "Address1_Name2": "",
                "Address1_Name3": "",
                "Address1_AddressLine1": "",
                "Address1_AddressLine2": "",
                "Address1_AddressLine3": "",
                "Address1_PostalCode": "",
                "Address1_City": "",
                "Address1_Country": "",
                "Address1_FaxNumber": "",
                "Address1_MobileNumber": "",
                "Address1_Telephone": "",
                "Address1_EmailID": ""
            },
            "Address_Info2": {
                "Address2_Title": "",
                "Address2_Name1": "",
                "Address2_Name2": "",
                "Address2_Name3": "",
                "Address2_AddressLine1": "",
                "Address2_AddressLine2": "",
                "Address2_AddressLine3": "",
                "Address2_PostalCode": "",
                "Address2_City": "",
                "Address2_Country": "",
                "Address2_FaxNumber": "",
                "Address2_MobileNumber": "",
                "Address2_Telephone": "",
                "Address2_EmailID": ""
            },
            "Address_Info3": {
                "Address3_Title": "",
                "Address3_Name1": "",
                "Address3_Name2": "",
                "Address3_Name3": "",
                "Address3_AddressLine1": "",
                "Address3_AddressLine2": "",
                "Address3_AddressLine3": "",
                "Address3_PostalCode": "",
                "Address3_City": "",
                "Address3_Country": "",
                "Address3_FaxNumber": "",
                "Address3_MobileNumber": "",
                "Address3_Telephone": "",
                "Address3_EmailID": ""
            },
            "Bank_Info1": {
                "Bank1_Country": "",
                "Bank1_BankName": "",
                "Bank1_SwiftCode": "",
                "Bank1_AccountNumber": "",
                "Bank1_AccountHolderName": "",
                "Bank1_IbanValue": ""
            },
            "Bank_Info2": {
                "Bank2_Country": "",
                "Bank2_BankName": "",
                "Bank2_SwiftCode": "",
                "Bank2_AccountNumber": "",
                "Bank2_AccountHolderName": "",
                "Bank2_IbanValue": ""
            },
            "Bank_Info3": {
                "Bank3_Country": "",
                "Bank3_BankName": "",
                "Bank3_SwiftCode": "",
                "Bank3_AccountNumber": "",
                "Bank3_AccountHolderName": "",
                "Bank3_IbanValue": ""
            },
            "Tax_Info": {
                "Tax_Number1": "",
                "Tax_Number2": "",
                "Tax_number3": "",
                "Tax_VatRegNumber": "",
                "Tax_Currency": "",
                "Tax_IncoTerms1": "",
                "Tax_IncoTerms2": "",
                "Tax_PlannedDeliveryDays": ""
            }
        }
    ];

    //IIS Path
    var RemoteURL = "http://localhost/RetailVendor";


    $(document).ready(function () {
        console.log("Vendor JS is ready");
        console.log(finalOutput);

        $("#Vendor_Address_Name1").attr('maxlength', 160);
        $("#Vendor_Address2_Name1").attr('maxlength', 160);
        $("#Vendor_Address3_Name1").attr('maxlength', 160);

        $("#Vendor_Address_Email").attr('maxlength', 240);
        $("#Vendor_Address2_Email").attr('maxlength', 240);
        $("#Vendor_Address3_Email").attr('maxlength', 240);

        $("#Vendor_Bank_Holder").attr('maxlength', 60);
        $("#Vendor_Bank2_Holder").attr('maxlength', 60);
        $("#Vendor_Bank3_Holder").attr('maxlength', 60);

        $("#Vendor_Address_Postal").attr('maxlength', 10);
        $("#Vendor_Address2_Postal").attr('maxlength', 10);
        $("#Vendor_Address3_Postal").attr('maxlength', 10);

        $("#Vendor_Address_City").attr('maxlength', 40);
        $("#Vendor_Address2_City").attr('maxlength', 40);
        $("#Vendor_Address3_City").attr('maxlength', 40);

        $("#Vendor_Bank_IBAN").attr('maxlength', 60);
        $("#Vendor_Bank2_IBAN").attr('maxlength', 60);
        $("#Vendor_Bank3_IBAN").attr('maxlength', 40);

        $("#Vendor_Tax_Vat").attr('maxlength', 20);

        $("#Vendor_Tax_Taxnum1").attr('maxlength', 16);
        $("#Vendor_Tax_Taxnum2").attr('maxlength', 11);
        $("#Vendor_Tax_Taxnum3").attr('maxlength', 18);
        $("#OTPValue").val("");

        //authenticate();
        //getSample();
        //getVendorDetails();
        //getVendorDetailsByEmail();
        //EmailVerification();

        //Alfresco Api's
        GetAlfrescoTicket();
        var tick = localStorage.getItem("alfrescoTicket");

        //loadJsonCompanyCodes();
        //loadJsonCountry();
        //loadJsonTitle();
        //loadJsonCurrency();
        //loadJsonIncoTerms1();
        createCaptcha();


        if (location.href.indexOf('VendorCreate') > 0) {
            $('#vendor_Loader').css('display', 'block');
            loadAllAlfresco(tick);
        }

        if (location.search !== null && location.search !== "") {
            update = location.search.split('?')[1].split('=')[1];
            if (update === "update")
                $('#vendor_Loader').css('display', 'block');

            console.log(update);
        }

        if (update === "update") {
            console.log("Get Ready");
            $('#vendor_Home_tabs').addClass('disabled');
            $('#vendor_Tabs_Content').addClass('disabled');
            setTimeout(GetJqueryVendor, 3000);
        }

        var targetTime = 60 * 1;
        var timer = $('#customer_update_Timer');

        if (location.href.indexOf('SubmitOTP') > 0) {
            startTimer(targetTime, timer);
        }

    });

    function loadAllAlfresco(tick) {
        if (tick !== null || tick !== "") {
            GetAlfrescoCompanyCodes();
            GetAlfrescoPurchaseOrg();
            GetAlfrescoCountries();
            GetAlfrescoTitles();
            GetAlfrescoCurrency();
            GetAlfrescoIncoTerms1();
        }
    }

    $("#Vendor_Address_Form1 input,#Vendor_Address2_Form input,#Vendor_Address3_Form input,#Vendor_Bank_Form1 input,#Vendor_Bank2_Form input,#Vendor_Bank3_Form input,#Vendor_Tax_Form input").keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });

    //$('#cpatchaTextBox').keyup(function (e) {
    //    $(this).val($(this).val());
    //    e.preventDefault();
    //});

    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text(minutes + ":" + seconds);

            if (--timer < 0) {
                // timer = duration;
                display.css('color', 'red');
                display.text("Expired!");
                window.setTimeout(function () {
                    window.location.href = '/OTP/GenerateOTP';
                }, 2000);
            }
        }, 1000);
    }

    function authenticate() {
        var ticket = '';

        $.ajax({
            url: "http://192.168.1.87:8080/alfresco/s/api/login.json?u=admin&pw=admin",
            method: 'GET',
            crossDomain: true,
            headers: { 'Content-Type': 'application/json' },
            dataType: "json",
            success: function (response) {
                console.log(response); // server response
            },
            error: function (error) {
                console.log(error);
            }

        });

        //$.get("http://192.168.1.87:8080/alfresco/s/api/login.json?u=admin&pw=admin", function (data, status) {
        //    console.log(data);
        //});

        //$.ajax({
        //    url: "http://192.168.1.87:8080/alfresco/s/api/login.json?u=admin&pw=admin",
        //    method: 'POST',
        //    type: "json",
        //    data: crediantials,
        //    success: function (data,status,error) {
        //        console.log(data); // server response
        //    },
        //    error: function (data, status, error) {
        //        console.log(error);
        //    }

        //});

        //$.ajax({
        //    url: 'http://192.168.1.87:8080/alfresco/s/api/login.json?u=admin&pw=admin',
        //    method: 'GET',
        //    crossDomain: true,
        //    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        //    type: 'json',
        //    //data: sendingData,
        //    success: function (response) {
        //        console.log(response);
        //    },
        //    error: function (error) {
        //        console.log(error);
        //    }
        //});
    }

    function GetAlfrescoTicket() {
        $.ajax({
            type: 'GET',
            //IIS Path
            url: RemoteURL + '/OTP/GetAlfrescoTicket',

            //Local Path
            //url: '/OTP/GetAlfrescoTicket',
            cache: false,
            success: function (result) {
                $.parseJSON(result);
                var response = $.parseJSON(result);
                localStorage.setItem("alfrescoTicket", response.data.ticket);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function GetAlfrescoCompanyCodes() {
        $.ajax({
            type: 'GET',
            //IIS Path
            url: RemoteURL + '/OTP/GetAlfrescoCompanyCodes',

            //Local
            //url: '/OTP/GetAlfrescoCompanyCodes',
            cache: false,
            success: function (result) {
                var response = $.parseJSON(result);
                console.log(response);
                localStorage.setItem("AlfrescoCompanyCodes", result);
                loadCompanyCodes(response, null, null);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function GetAlfrescoPurchaseOrg() {
        $.ajax({
            type: 'GET',
            //IIS Path
            url: RemoteURL + '/OTP/GetAlfrescoPurchaseOrg',

            //Local
            //url: '/OTP/GetAlfrescoPurchaseOrg',
            cache: false,
            success: function (result) {
                var response = $.parseJSON(result);
                console.log(response);
                localStorage.setItem("PurchaseOrgList", JSON.stringify(response));
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function GetAlfrescoCountries() {
        $.ajax({
            type: 'GET',
            //IIS Path
            url: RemoteURL + '/OTP/GetAlfrescoCountries',

            //Local
            //url: '/OTP/GetAlfrescoCountries',
            cache: false,
            success: function (result) {
                var response = $.parseJSON(result);
                response.sort(function (a, b) {
                    return a.data < b.data ? -1 : a.data > b.data ? 1 : 0;
                });
                localStorage.setItem('AlfrescoCountries', result);
                loadCountry(response, null, null);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function sorting(places) {
        places.sort(function (a, b) {
            return a.data < b.data ? -1 : a.data > b.data ? 1 : 0;
        });
    }

    function GetAlfrescoTitles() {
        $.ajax({
            type: 'GET',
            //IIS Path
            url: RemoteURL + '/OTP/GetAlfrescoTitles',

            //Local
            //url: '/OTP/GetAlfrescoTitles',
            cache: false,
            success: function (result) {
                var response = $.parseJSON(result);
                console.log(response);
                loadTitle(response);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function GetAlfrescoCurrency() {
        $.ajax({
            type: 'GET',
            //IIS Path
            url: RemoteURL + '/OTP/GetAlfrescoCurrency',

            //Local
            //url: '/OTP/GetAlfrescoCurrency',
            cache: false,
            success: function (result) {
                var response = $.parseJSON(result);
                localStorage.setItem("AlfrescoCurrency", result);
                console.log(response);
                loadCurrency(response, null, null);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function GetAlfrescoIncoTerms1() {
        $.ajax({
            type: 'GET',
            //IIS Path
            url: RemoteURL + '/OTP/GetAlfrescoIncoTerms1',

            //Local
            //url: '/OTP/GetAlfrescoIncoTerms1',
            cache: false,
            success: function (result) {
                var response = $.parseJSON(result);
                console.log(response);
                localStorage.setItem("AlfrescoIncoTerms1", result);
                loadIncoTerms1(response, null, null);
                if (update === null)
                    $('#vendor_Loader').css('display', 'none');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    $('#resendOTP').click(function () {

        window.location.href = '/OTP/GenerateOTP';

        //$.ajax({
        //    url: '/OTP/GenerateOTP',
        //    type: "GET",
        //    contentType: false, // Not to set any content header  
        //    processData: false, // Not to process data  
        //    success: function (result) {
        //        window.location.href = '/OTP/GenerateOTP';
        //    },
        //    error: function (err) {
        //        alert(err.statusText);
        //    }
        //});
    });

    $("#vendor_Company_Tab").click(function () {

        if ($("#vendor_Company_Tab").hasClass('active')) {
            $("#vendor_Company_Tab").attr('href', '#vendor_CompanyInfo');
            $("#vendor_Company_Tab").css("pointer-events", "auto");
            $("#vendor_CompanyInfo").addClass('show active');
            $("#vendor_AddressInfo").removeClass('show');

            $("#vendor_AddressInfo").removeClass('show active');
            $("#vendor_BankInfo").removeClass('show active');
            $("#vendor_TaxInfo").removeClass('show active');
            $("#vendor_DocumentsInfo").removeClass('show active');
        }
    });

    $("#vendor_Address_Tab").click(function () {

        if ($("#vendor_Address_Tab").hasClass('active')) {
            $("#vendor_Address_Tab").attr('href', '#vendor_AddressInfo');
            $("#vendor_Address_Tab").css("pointer-events", "auto");
            $("#vendor_AddressInfo").addClass('show active');

            $("#vendor_CompanyInfo").removeClass('show active');
            $("#vendor_BankInfo").removeClass('show active');
            $("#vendor_TaxInfo").removeClass('show active');
            $("#vendor_DocumentsInfo").removeClass('show active');
        }
    });

    $("#vendor_Bank_Tab").click(function () {

        if ($("#vendor_Bank_Tab").hasClass('active')) {
            $("#vendor_Bank_Tab").attr('href', '#vendor_AddressInfo');
            $("#vendor_Bank_Tab").css("pointer-events", "auto");
            $("#vendor_BankInfo").addClass('show active');

            $("#vendor_CompanyInfo").removeClass('show active');
            $("#vendor_AddressInfo").removeClass('show active');
            $("#vendor_TaxInfo").removeClass('show active');
            $("#vendor_DocumentsInfo").removeClass('show active');
        }
    });

    $("#vendor_Tax_Tab").click(function () {

        if ($("#vendor_Tax_Tab").hasClass('active')) {
            $("#vendor_Tax_Tab").attr('href', '#vendor_AddressInfo');
            $("#vendor_Tax_Tab").css("pointer-events", "auto");
            $("#vendor_TaxInfo").addClass('show active');

            $("#vendor_CompanyInfo").removeClass('show active');
            $("#vendor_AddressInfo").removeClass('show active');
            $("#vendor_BankInfo").removeClass('show active');
            $("#vendor_DocumentsInfo").removeClass('show active');
        }
    });

    $("#vendor_Documents_Tab").click(function () {

        if ($("#vendor_Documents_Tab").hasClass('active')) {
            $("#vendor_Documents_Tab").attr('href', '#vendor_AddressInfo');
            $("#vendor_Documents_Tab").css("pointer-events", "auto");
            $("#vendor_DocumentsInfo").addClass('show active');

            $("#vendor_CompanyInfo").removeClass('show active');
            $("#vendor_AddressInfo").removeClass('show active');
            $("#vendor_BankInfo").removeClass('show active');
            $("#vendor_TaxInfo").removeClass('show active');
        }
    });

    $('#Vendor_Profile_Form').submit(function (e) {
        var companyCode = $("#Vendor_Profile_CompanyCodeDDL_ErrorMsg");
        var errorMsgCompCode = "";
        companyCode.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Profile_CompanyCodeDDL').val() === "" || $('#Vendor_Profile_CompanyCodeDDL').val() === null) {
            errorMsgCompCode = "Please select company code";
            companyCode.append(errorMsgCompCode);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgCompCode = "";
            companyCode.css('display', 'none');
            e.preventDefault();
        }


        var purchaseOrg = $("#Vendor_Profile_PurchaseOrgDDL_ErrorMsg");
        var errorMsgPurOrg = "";
        purchaseOrg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Profile_PurchaseOrgDDL').val() === "" || $('#Vendor_Profile_PurchaseOrgDDL').val() === null) {
            errorMsgPurOrg = "Please select purchase organization";
            purchaseOrg.append(errorMsgPurOrg);
            purchaseOrg.css('display', 'block');
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgPurOrg = "";
            //purchaseOrg.css('display', 'none');
            e.preventDefault();
        }

        if ($('#Vendor_Profile_PurchaseOrgDDL').val() !== "" && $('#Vendor_Profile_PurchaseOrgDDL').val() !== null && $('#Vendor_Profile_CompanyCodeDDL').val() !== "" && $('#Vendor_Profile_CompanyCodeDDL').val() !== null) {

            finalOutput[0].Company_Info.CompanyCode = Number.parseInt($('#Vendor_Profile_CompanyCodeDDL').val());
            finalOutput[0].Company_Info.PurchaseOrg = $('#Vendor_Profile_PurchaseOrgDDL').val();

            $("#vendor_CompanyInfo").removeClass('show active');
            //$('#vendor_Company_Tab').attr('aria-selected', 'false');
            $("#vendor_AddressInfo").addClass('show active');
            $("#vendor_Address_Tab").addClass('active');
            $("#vendor_Company_Tab").css("pointer-events", "auto");
            $("#vendor_Address_Tab").css("pointer-events", "auto");
            //$('#vendor_Address_Tab').attr('aria-selected', 'true');
            e.preventDefault();
        }
        event.preventDefault();
    });

    function loadCompanyCodes(comapanyCodes, selectedCompanyCode, update = "") {
        var html_code = '';

        html_code += '<option value="" disabled selected>Select company code</option>';

        $.each(comapanyCodes, function (key, value) {

            if (update === "update" && selectedCompanyCode !== null && selectedCompanyCode !== "" && selectedCompanyCode === value.company_code) {
                html_code += '<option selected value="' + value.company_code + '">' + value.id + '</option>';
            }
            else
                html_code += '<option value="' + value.company_code + '">' + value.id + '</option>';

        });

        $("#Vendor_Profile_CompanyCodeDDL").html(html_code);

    }

    function loadPurchaseOrganizations(companyCode, purchaseOrg = "", update = "") {
        var html_code = '';
        var obj = localStorage.getItem("PurchaseOrgList");
        var purchaseOrgList = JSON.parse(obj);

        html_code += '<option value="" disabled selected>Select company code</option>';

        $.each(purchaseOrgList, function (key, value) {

            //if (value.company_code === companyCode) {
            //    html_code += '<option value="' + value.company_code + '">' + value.id + '</option>';
            //}

            if (value.r_company_code + "" === companyCode) {

                if (update === "update" && value.purchase_org + "" === purchaseOrg && purchaseOrg !== null && purchaseOrg !== "") {
                    html_code += '<option selected value="' + value.purchase_org + '">' + value.data + '</option>';
                }
                else {
                    html_code += '<option value="' + value.purchase_org + '">' + value.data + '</option>';
                }

            }

        });

        $("#Vendor_Profile_PurchaseOrgDDL").html(html_code);

    }

    function loadJsonCompanyCodes(companyCode, update = "") {
        var html_code = '';
        $.getJSON('../Json/CompanyCode.json', function (data) {

            html_code += '<option value="" disabled selected>Select company code</option>';

            $.each(data, function (key, value) {

                if (update === "update" && companyCode !== null && companyCode !== "") {
                    html_code += '<option selected value="' + value.company_code + '">' + value.id + '</option>';
                }
                else {
                    html_code += '<option value="' + value.company_code + '">' + value.id + '</option>';
                }

            });

            $("#Vendor_Profile_CompanyCodeDDL").html(html_code);
        });
    }

    $(document).on('change', '#Vendor_Profile_CompanyCodeDDL', function () {
        var companyCode = $(this).val();

        if (companyCode !== '' && companyCode !== null) {
            $('#Vendor_Profile_CompanyCodeDDL_ErrorMsg').text('');
            loadPurchaseOrganizations(companyCode, null, null);
            //loadJsonPurchaseOrg(companyCode, null, null);
        }
        else {
            $("#Vendor_Profile_PurchaseOrgDDL").html('<option value="">Select Purchase Org</option>');
        }

    });

    $(document).on('change', '#Vendor_Profile_PurchaseOrgDDL', function () {
        var purchaseOrg = $(this).val();

        if (purchaseOrg !== '' && purchaseOrg !== null) {
            $('#Vendor_Profile_PurchaseOrgDDL_ErrorMsg').text('');
        }

    });

    function loadJsonPurchaseOrg(companyCode, purchaseOrg = "", update = "") {
        var html_code = '';

        $.getJSON('../Json/PurchaseOrg.json', function (data) {

            html_code += '<option value="" disabled selected>Select Purchase Organization</option>';

            $.each(data, function (key, value) {

                if (value.r_company_code + "" === companyCode) {

                    if (update === "update" && value.purchase_org + "" === purchaseOrg) {
                        html_code += '<option selected value="' + value.purchase_org + '">' + value.data + '</option>';
                    }
                    else {
                        html_code += '<option value="' + value.purchase_org + '">' + value.data + '</option>';
                    }

                }

            });

            $("#Vendor_Profile_PurchaseOrgDDL").html(html_code);
        });
    }


    ///////////////////////////////////////////////////  Address Info Screen  //////////////////////////////////////////////////////////////////////////////

    function loadCountry(countries, selectedCountry, update = "") {
        var html_code = '';

        html_code += '<option value="" disabled selected>Select Country</option>';

        $.each(countries, function (key, value) {

            if (value.country !== "" || value.country !== null) {
                if (update === "update" && selectedCountry !== null && selectedCountry !== "" && selectedCountry === value.country) {
                    html_code += '<option selected value="' + value.country + '">' + value.data + '</option>';
                }
                else {
                    html_code += '<option value="' + value.country + '">' + value.data + '</option>';
                }
            }

        });

        $("#Vendor_Address_CountryDDL").html(html_code);
        $("#Vendor_Address2_CountryDDL").html(html_code);
        $("#Vendor_Address3_CountryDDL").html(html_code);
        $("#Vendor_Bank_CountryDDL").html(html_code);
        $("#Vendor_Bank2_CountryDDL").html(html_code);
        $("#Vendor_Bank3_CountryDDL").html(html_code);
    }

    $(document).on('change', '#Vendor_Address_TitleDDL,#Vendor_Address_CountryDDL', function () {
        var id = $(this)[0].id;
        var value = $(this).val();

        if (id === 'Vendor_Address_TitleDDL') {
            if (value !== '' && value !== null)
                $('#Vendor_Address_TitleDDL_ErrorMsg').text('');
        }
        else {
            if (value !== '' && value !== null) {
                $('#Vendor_Address_CountryDDL_ErrorMsg').text('');
                $("#Vendor_Address_Postal").val('');
                postalCodeValidation(value);
            }

        }

    });

    function postalCodeValidation(countryName) {
        var list = localStorage.getItem('AlfrescoCountries');
        var countriesList = $.parseJSON(list);

        $.each(countriesList, function (key, value) {
            if (value.country === countryName && value.postalCode_maxLength !== null) {
                $("#Vendor_Address_Postal").attr('maxlength', value.postalCode_maxLength);
            }

            if (value.country === countryName && value.postalCode_IsNumeric === "NumericOnly") {
                $("#Vendor_Address_Postal").keypress(function (e) {
                    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
                });
            }
        });

    }

    function loadJsonCountry() {
        var html_code = '';

        $.getJSON('../Json/Country.json', function (data) {

            html_code += '<option value="" disabled selected>Select Country</option>';

            $.each(data, function (key, value) {

                if (value.country !== "" || value.country !== null) {
                    html_code += '<option value="' + value.country + '">' + value.data + '</option>';
                }

            });

            $("#Vendor_Address_CountryDDL").html(html_code);
            $("#Vendor_Address2_CountryDDL").html(html_code);
            $("#Vendor_Address3_CountryDDL").html(html_code);
            $("#Vendor_Bank_CountryDDL").html(html_code);
            $("#Vendor_Bank2_CountryDDL").html(html_code);
            $("#Vendor_Bank3_CountryDDL").html(html_code);
        });
    }

    function loadTitle(titles) {
        var html_code = '';

        html_code += '<option value="" disabled selected>Select Title</option>';

        $.each(titles, function (key, value) {

            if (value.title !== "" || value.order_currency !== null) {
                html_code += '<option value="' + value.title + '">' + value.title + '</option>';
            }

        });

        $("#Vendor_Address_TitleDDL").html(html_code);
        $("#Vendor_Address2_TitleDDL").html(html_code);
        $("#Vendor_Address3_TitleDDL").html(html_code);
    }

    function loadJsonTitle() {
        var html_code = '';

        $.getJSON('../Json/Title.json', function (data) {

            html_code += '<option value="" disabled selected>Select Title</option>';

            $.each(data, function (key, value) {

                if (value.title !== "" || value.order_currency !== null) {
                    html_code += '<option value="' + value.title + '">' + value.title + '</option>';
                }

            });

            $("#Vendor_Address_TitleDDL").html(html_code);
            $("#Vendor_Address2_TitleDDL").html(html_code);
            $("#Vendor_Address3_TitleDDL").html(html_code);
        });
    }

    $('#Vendor_Address_Line2,#Vendor_Address_Line3,#Vendor_Address2_Line2,#Vendor_Address3_Line2,#Vendor_Address3_Line3').focusout(function (e) {
        var id = $(this)[0].id;

        switch (id) {
            case "Vendor_Address_Line2":
                if ($('#Vendor_Address_Line2').val() !== null && $('#Vendor_Address_Line2').val() !== "")
                    finalOutput[0].Address_Info1.Address1_AddressLine2 = $('#Vendor_Address_Line2').val();
                break;
            case "Vendor_Address_Line3":
                if ($('#Vendor_Address_Line3').val() !== null && $('#Vendor_Address_Line3').val() !== "")
                    finalOutput[0].Address_Info1.Address1_AddressLine3 = $('#Vendor_Address_Line3').val();
                break;
            case "Vendor_Address2_Line2":
                if ($('#Vendor_Address2_Line2').val() !== null && $('#Vendor_Address2_Line2').val() !== "")
                    finalOutput[0].Address_Info2.Address2_AddressLine2 = $('#Vendor_Address2_Line2').val();
                break;
            case "Vendor_Address2_Line3":
                if ($('#Vendor_Address2_Line3').val() !== null && $('#Vendor_Address2_Line3').val() !== "")
                    finalOutput[0].Address_Info2.Address2_AddressLine3 = $('#Vendor_Address2_Line3').val();
                break;
            case "Vendor_Address3_Line2":
                if ($('#Vendor_Address3_Line2').val() !== null && $('#Vendor_Address3_Line2').val() !== "")
                    finalOutput[0].Address_Info3.Address3_AddressLine2 = $('#Vendor_Address3_Line2').val();
                break;
            case "Vendor_Address3_Line3":
                if ($('#Vendor_Address3_Line3').val() !== null && $('#Vendor_Address3_Line3').val() !== "")
                    finalOutput[0].Address_Info3.Address3_AddressLine3 = $('#Vendor_Address3_Line3').val();
                break;
        }

    });

    $('#Vendor_Address_Line1,#Vendor_Address2_Line1,#Vendor_Address3_Line1').focusout(function (e) {
        var addInfo = "";
        var id = $(this)[0].id;

        if (id === 'Vendor_Address_Line1') {
            addInfo = $('#Vendor_Address_Line1').val();

            var addressLine1Msg = $("#Vendor_Address_Line1_ErrorMsg");
            var errorMsgAddressLine1 = "";
            addressLine1Msg.html('<div class="vendorError" style="display: block;"></div>');

            if ($('#Vendor_Address_Line1').val() === "" || $('#Vendor_Address_Line1').val() === null) {
                errorMsgAddressLine1 = "Please enter address";
                addressLine1Msg.append(errorMsgAddressLine1);
                finalOutput[0].Address_Info1.Address1_AddressLine1 = $('#Vendor_Address_Line1').val();
                e.preventDefault();
                //return false;
            }
            else {
                errorMsgAddressLine1 = "";
                addressLine1Msg.css('display', 'none');
                e.preventDefault();
            }

            if (addInfo !== null || addInfo !== "")
                $('#Vendor_Address1_Info').text(addInfo);
        }

        if (id === 'Vendor_Address2_Line1') {
            addInfo = $('#Vendor_Address2_Line1').val();

            var address2Line1Msg = $("#Vendor_Address2_Line1_ErrorMsg");
            var errorMsgAddress2Line1 = "";
            address2Line1Msg.html('<div class="vendorError" style="display: block;"></div>');

            if ($('#Vendor_Address2_Line1').val() === "" || $('#Vendor_Address2_Line1').val() === null) {
                errorMsgAddress2Line1 = "Please enter address";
                address2Line1Msg.append(errorMsgAddress2Line1);
                finalOutput[0].Address_Info2.Address2_AddressLine1 = $('#Vendor_Address2_Line1').val();
                e.preventDefault();
                //return false;
            }
            else {
                errorMsgAddress2Line1 = "";
                address2Line1Msg.css('display', 'none');
                e.preventDefault();
            }

            if (addInfo !== null || addInfo !== "")
                $('#Vendor_Address2_Info').text(addInfo);
        }

        if (id === 'Vendor_Address3_Line1') {
            addInfo = $('#Vendor_Address3_Line1').val();

            var address3Line1Msg = $("#Vendor_Address3_Line1_ErrorMsg");
            var errorMsgAddress3Line1 = "";
            address3Line1Msg.html('<div class="vendorError" style="display: block;"></div>');

            if ($('#Vendor_Address3_Line1').val() === "" || $('#Vendor_Address3_Line1').val() === null) {
                errorMsgAddress3Line1 = "Please enter address";
                address3Line1Msg.append(errorMsgAddress3Line1);
                finalOutput[0].Address_Info3.Address3_AddressLine1 = $('#Vendor_Address3_Line1').val();
                e.preventDefault();
                //return false;
            }
            else {
                errorMsgAddress3Line1 = "";
                address3Line1Msg.css('display', 'none');
                e.preventDefault();
            }

            if (addInfo !== null || addInfo !== "")
                $('#Vendor_Address3_Info').text(addInfo);
        }

    });

    var selectedClearId = "";
    var ClearAllFields = false;

    $('#modalClear').click(function () {
        ClearAllFields = true;

        if (selectedClearId === 'vendor_Address1_Clear' && ClearAllFields) {
            $('#Vendor_Address_Form1')[0].reset();
            $('#Vendor_Address1_Info').text("Add Address Information");

            $('#Vendor_Address_TitleDDL_ErrorMsg,#Vendor_Address_Name1_ErrorMsg,#Vendor_Address_Line1_ErrorMsg,#Vendor_Address_Line2_ErrorMsg,#Vendor_Address_Line3_ErrorMsg,#Vendor_Address_Postal_ErrorMsg,#Vendor_Address_City_ErrorMsg,#Vendor_Address_CountryDDL_ErrorMsg,#Vendor_Address_Fax_ErrorMsg,#Vendor_Address_Mobile_ErrorMsg,#Vendor_Address_Telephone_ErrorMsg,#Vendor_Address_Email_ErrorMsg').text();
            $('#collapseOne').addClass('show');
        }

        if (selectedClearId === 'vendor_Address2_Clear' && ClearAllFields) {
            $('#Vendor_Address2_Form')[0].reset();
            $('#Vendor_Address2_Info').text("Add Address Information");

            $('#Vendor_Address2_TitleDDL_ErrorMsg,#Vendor_Address2_Name1_ErrorMsg,#Vendor_Address2_Line1_ErrorMsg,#Vendor_Address2_Line2_ErrorMsg,#Vendor_Address2_Line3_ErrorMsg,#Vendor_Address2_Postal_ErrorMsg,#Vendor_Address2_City_ErrorMsg,#Vendor_Address2_Fax_ErrorMsg,#Vendor_Address2_Mobile_ErrorMsg,#Vendor_Address2_Telephone_ErrorMsg,#Vendor_Address2_Email_ErrorMsg').text();
            $('#collapseTwo').addClass('show');

        }

        if (selectedClearId === 'vendor_Address3_Clear' && ClearAllFields) {
            $('#Vendor_Address3_Form')[0].reset();
            $('#Vendor_Address3_Info').text("Add Address Information");

            $('#Vendor_Address3_TitleDDL_ErrorMsg,#Vendor_Address3_Name1_ErrorMsg,#Vendor_Address3_Line1_ErrorMsg,#Vendor_Address3_Line2_ErrorMsg,#Vendor_Address3_Line3_ErrorMsg,#Vendor_Address3_Postal_ErrorMsg,#Vendor_Address3_City_ErrorMsg,#Vendor_Address3_CountryDDL_ErrorMsg,#Vendor_Address3_Fax_ErrorMsg,#Vendor_Address3_Mobile_ErrorMsg,#Vendor_Address3_Telephone_ErrorMsg,#Vendor_Address3_Email_ErrorMsg').text();
            $('#modal-delete').modal('hide');
            $('#collapseThree').addClass('show');
        }

        if (selectedClearId === 'vendor_Bank_Clear' && ClearAllFields) {
            $('#Vendor_Bank_Form1')[0].reset();

            $('#Vendor_Bank_CountryDDL_ErrorMsg,#Vendor_Bank_NameDDL_ErrorMsg,#Vendor_Bank_SwiftDDL_ErrorMsg,#Vendor_Bank_Account_ErrorMsg,#Vendor_Bank_Holder_ErrorMsg,#Vendor_Bank_IBAN_ErrorMsg').text();
            $('#modal-delete').modal('hide');
            $('#bankcollapseOne').addClass('show');
        }

        if (selectedClearId === 'vendor_Bank2_Clear' && ClearAllFields) {
            $('#Vendor_Bank2_Form')[0].reset();

            $('#Vendor_Bank2_CountryDDL_ErrorMsg,#Vendor_Bank2_NameDDL_ErrorMsg,#Vendor_Bank2SwiftDDL_ErrorMsg,#Vendor_Bank2_Account_ErrorMsg,#Vendor_Bank2_Holder_ErrorMsg,#Vendor_Bank2_IBAN_ErrorMsg').text();
            $('#modal-delete').modal('hide');
            $('#bankcollapseTwo').addClass('show');
        }

        if (selectedClearId === 'vendor_Bank3_Clear' && ClearAllFields) {
            $('#Vendor_Bank3_Form')[0].reset();

            $('#Vendor_Bank3_CountryDDL_ErrorMsg,#Vendor_Bank3_NameDDL_ErrorMsg,#Vendor_Bank3_SwiftDDL_ErrorMsg,#Vendor_Bank3_Account_ErrorMsg,#Vendor_Bank3_Holder_ErrorMsg,#Vendor_Bank3_IBAN_ErrorMsg').text();
            $('#modal-delete').modal('hide');
            $('#bankcollapseThree').addClass('show');
        }

        if (selectedClearId === 'vendor_Tax_Clear' && ClearAllFields) {
            $('#Vendor_Tax_Form')[0].reset();

            $('#Vendor_Tax_Taxnum1_ErrorMsg,#Vendor_Tax_Taxnum2_ErrorMsg,#Vendor_Tax_Taxnum3_ErrorMsg,#Vendor_Tax_Vat_ErrorMsg,#Vendor_Tax_Order_ErrorMsg,#Vendor_Tax_Incoterms1DDL_ErrorMsg,#Vendor_Tax_Incoterms2_ErrorMsg,#Vendor_Tax_Delivery_ErrorMsg').text();
            $('#modal-delete').modal('hide');
        }

        if (selectedClearId === 'vendor_Document_Clear' && ClearAllFields) {
            $('#Vendor_Document_Form')[0].reset();

            $('#Vendor_Doc_Article_ErrorMsg,#Vendor_Doc_Commercial_ErrorMsg,#Vendor_Doc_Statement_ErrorMsg,#Vendor_Doc_Authorised_ErrorMsg,#Vendor_Doc_Contract_ErrorMsg,#Vendor_Doc_Balance_ErrorMsg,#Vendor_Doc_Civil_ErrorMsg,#Vendor_Doc_Comment_ErrorMsg').text();
            $('#modal-delete').modal('hide');
        }

        $('#modal-delete').modal('hide');

    });

    $('#vendor_Address1_Clear,#vendor_Address2_Clear,#vendor_Address3_Clear,#vendor_Bank_Clear,#vendor_Bank2_Clear,#vendor_Bank3_Clear,#vendor_Tax_Clear,#vendor_Document_Clear').click(function () {

        $('#modal-delete').modal('show');
        selectedClearId = this.id;

    });

    $('#addressPreviousBtn1,#addressPreviousBtn2,#addressPreviousBtn3').click(function () {
        $('#vendor_AddressInfo').removeClass('show active');
        $('#vendor_CompanyInfo').addClass('show active');
    });

    $('#Vendor_Address1_Add').click(function () {
        $('#vendor_Address_Accordion2').css('display', 'block');
        $('#collapseTwo').addClass('show');
        $('#Vendor_Address1_Add').css('display', 'none');
        //$('#collapseOne').removeClass('show');
    });

    $('#Vendor_Address2_Add').click(function () {
        $('#vendor_Address_Accordion3').css('display', 'block');
        $('#collapseThree').addClass('show');
        $('#Vendor_Address2_Add').css('display', 'none');
    });

    $('#Vendor_Address2_Delete').click(function () {
        // $('#collapseTwo').removeClass('show');
        $('#vendor_Address_Accordion2').css('display', 'none');
        $('#Vendor_Address2_Form')[0].reset();
        $('#Vendor_Address1_Add').css('display', 'inline-block');
        $('#collapseOne').addClass('show');

        $('#Vendor_Address2_TitleDDL_ErrorMsg,#Vendor_Address2_Name1_ErrorMsg,#Vendor_Address2_Line1_ErrorMsg,Vendor_Address2_Line2_ErrorMsg,#Vendor_Address2_Line3_ErrorMsg,#Vendor_Address2_Postal_ErrorMsg,#Vendor_Address2_City_ErrorMsg,#Vendor_Address2_CountryDDL_ErrorMsg,#Vendor_Address2_Fax_ErrorMsg,#Vendor_Address2_Mobile_ErrorMsg,#Vendor_Address2_Telephone_ErrorMsg,#Vendor_Address2_Email_ErrorMsg').text("");
    });

    $('#Vendor_Address3_Delete').click(function () {
        //$('#collapseThree').removeClass('show');
        $('#vendor_Address_Accordion3').css('display', 'none');
        $('#Vendor_Address3_Form')[0].reset();
        $('#Vendor_Address2_Add').css('display', 'inline-block');
        $('#collapseOne').addClass('show');

        $('#Vendor_Address3_TitleDDL_ErrorMsg,#Vendor_Address3_Name1_ErrorMsg,#Vendor_Address3_Line1_ErrorMsg,#Vendor_Address3_Line2_ErrorMsg,#Vendor_Address3_Line3_ErrorMsg,#Vendor_Address3_Postal_ErrorMsg,#Vendor_Address3_City_ErrorMsg,#Vendor_Address3_CountryDDL_ErrorMsg,#Vendor_Address3_Fax_ErrorMsg,#Vendor_Address3_Mobile_ErrorMsg,#Vendor_Address3_Telephone_ErrorMsg,#Vendor_Address3_Email_ErrorMsg').text("");
    });

    $('#Vendor_Address_Name1,#Vendor_Address2_Name1,#Vendor_Address3_Name1,#Vendor_Address_Postal,#Vendor_Address2_Postal,#Vendor_Address3_Postal,#Vendor_Address_City,#Vendor_Address2_City,#Vendor_Address3_City,#Vendor_Address_Fax,#Vendor_Address2_Fax,#Vendor_Address3_Fax,#Vendor_Address_Mobile,#Vendor_Address2_Mobile,#Vendor_Address3_Mobile,#Vendor_Address_Telephone,#Vendor_Address2_Telephone,#Vendor_Address3_Telephone,#Vendor_Address_Email,#Vendor_Address2_Email,#Vendor_Address3_Email').focusout(function (e) {

        var id = $(this)[0].id;

        switch (id) {
            case "Vendor_Address_Name1":
                var Name1Msg = $("#Vendor_Address_Name1_ErrorMsg");
                var errorMsgName1 = "";
                Name1Msg.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address_Name1').val() === "" || $('#Vendor_Address_Name1').val() === null) {
                    errorMsgName1 = "Please enter name";
                    Name1Msg.append(errorMsgName1);
                    e.preventDefault();
                    //return false;
                }
                else {
                    var name1 = $('#Vendor_Address_Name1').val();

                    let subStr = new RegExp('.{1,' + 35 + '}', 'g');
                    var splitName1 = name1.match(subStr);
                    var finalName1 = splitName1[0];
                    var finalName2 = splitName1[1];
                    var finalName3 = "";

                    for (var i = 2; i < splitName1.length; i++) {
                        finalName3.concat(splitName1[i]);
                    }

                    finalOutput[0].Address_Info1.Address1_Name1 = finalName1;
                    finalOutput[0].Address_Info1.Address1_Name2 = finalName2;
                    finalOutput[0].Address_Info1.Address1_Name3 = finalName3;

                    errorMsgName1 = "";
                    Name1Msg.css('display', 'none');
                    e.preventDefault();
                }

                break;
            case "Vendor_Address2_Name1":

                var Name12Msg = $("#Vendor_Address2_Name1_ErrorMsg");
                var errorMsgName12 = "";
                Name12Msg.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address2_Name1').val() === "" || $('#Vendor_Address2_Name1').val() === null) {
                    errorMsgName12 = "Please enter name";
                    Name12Msg.append(errorMsgName12);
                    e.preventDefault();
                    //return false;
                }
                else {
                    var name12 = $('#Vendor_Address2_Name1').val();

                    let subStr = new RegExp('.{1,' + 35 + '}', 'g');
                    var splitName12 = name12.match(subStr);
                    var finalName12 = splitName12[0];
                    var finalName22 = splitName12[1];
                    var finalName32 = "";

                    for (var j = 2; j < splitName12.length; j++) {
                        finalName32.concat(splitName12[i]);
                    }

                    finalOutput[0].Address_Info2.Address2_Name1 = finalName12;
                    finalOutput[0].Address_Info2.Address2_Name2 = finalName22;
                    finalOutput[0].Address_Info2.Address2_Name3 = finalName32;

                    errorMsgName12 = "";
                    Name12Msg.css('display', 'none');
                    e.preventDefault();
                }

                break;
            case "Vendor_Address3_Name1":

                var Name123Msg = $("#Vendor_Address3_Name1_ErrorMsg");
                var errorMsgName123 = "";
                Name123Msg.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address3_Name1').val() === "" || $('#Vendor_Address3_Name1').val() === null) {
                    errorMsgName123 = "Please enter name";
                    Name123Msg.append(errorMsgName123);
                    e.preventDefault();
                    //return false;
                }
                else {
                    var name123 = $('#Vendor_Address3_Name1').val();

                    let subStr = new RegExp('.{1,' + 35 + '}', 'g');
                    var splitName123 = name123.match(subStr);
                    var finalName123 = splitName123[0];
                    var finalName223 = splitName123[1];
                    var finalName323 = "";

                    for (var k = 2; k < splitName123.length; k++) {
                        finalName323.concat(splitName123[i]);
                    }

                    finalOutput[0].Address_Info3.Address3_Name1 = finalName123;
                    finalOutput[0].Address_Info3.Address3_Name2 = finalName223;
                    finalOutput[0].Address_Info3.Address3_Name3 = finalName323;

                    errorMsgName123 = "";
                    Name123Msg.css('display', 'none');
                    e.preventDefault();
                }

                break;
            case "Vendor_Address_Postal":
                var postalCodeMsg = $("#Vendor_Address_Postal_ErrorMsg");
                var errorMsgPostalCode = "";
                postalCodeMsg.html('<div class="vendorError" style="display: block;"></div>');

                //if ($('#Vendor_Address_CountryDDL').val() !== null && $('#Vendor_Address_CountryDDL').val() !== "") {
                //    postalCodeValidation($('#Vendor_Address_CountryDDL').val());
                //}

                if ($('#Vendor_Address_Postal').val() === "" || $('#Vendor_Address_Postal').val() === null) {
                    errorMsgPostalCode = "Please enter postal code";
                    postalCodeMsg.append(errorMsgPostalCode);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgPostalCode = "";
                    postalCodeMsg.css('display', 'none');
                    finalOutput[0].Address_Info1.Address1_PostalCode = $('#Vendor_Address_Postal').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Address2_Postal":

                var postalCodeMsg2 = $("#Vendor_Address2_Postal_ErrorMsg");
                var errorMsgPostalCode2 = "";
                postalCodeMsg2.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address2_Postal').val() === "" || $('#Vendor_Address2_Postal').val() === null) {
                    errorMsgPostalCode2 = "Please enter postal code";
                    postalCodeMsg2.append(errorMsgPostalCode2);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgPostalCode2 = "";
                    postalCodeMsg2.css('display', 'none');
                    finalOutput[0].Address_Info2.Address2_PostalCode = $('#Vendor_Address2_Postal').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Address3_Postal":

                var postalCodeMsg3 = $("#Vendor_Address3_Postal_ErrorMsg");
                var errorMsgPostalCode3 = "";
                postalCodeMsg3.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address3_Postal').val() === "" || $('#Vendor_Address3_Postal').val() === null) {
                    errorMsgPostalCode3 = "Please enter postal code";
                    postalCodeMsg3.append(errorMsgPostalCode3);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgPostalCode3 = "";
                    postalCodeMsg3.css('display', 'none');
                    finalOutput[0].Address_Info3.Address3_PostalCode = $('#Vendor_Address3_Postal').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Address_City":

                var cityMsg = $("#Vendor_Address_City_ErrorMsg");
                var errorMsgCity = "";
                cityMsg.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address_City').val() === "" || $('#Vendor_Address_City').val() === null) {
                    errorMsgCity = "Please enter city";
                    cityMsg.append(errorMsgCity);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgCity = "";
                    cityMsg.css('display', 'none');
                    finalOutput[0].Address_Info1.Address1_City = $('#Vendor_Address_City').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Address2_City":

                var cityMsg2 = $("#Vendor_Address2_City_ErrorMsg");
                var errorMsgCity2 = "";
                cityMsg2.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address2_City').val() === "" || $('#Vendor_Address2_City').val() === null) {
                    errorMsgCity2 = "Please enter city";
                    cityMsg2.append(errorMsgCity2);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgCity2 = "";
                    cityMsg2.css('display', 'none');
                    finalOutput[0].Address_Info2.Address2_City = $('#Vendor_Address2_City').val();
                    e.preventDefault();
                }
                break;
            case "Vendor_Address3_City":

                var cityMsg3 = $("#Vendor_Address3_City_ErrorMsg");
                var errorMsgCity3 = "";
                cityMsg3.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address3_City').val() === "" || $('#Vendor_Address3_City').val() === null) {
                    errorMsgCity3 = "Please enter city";
                    cityMsg3.append(errorMsgCity3);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgCity3 = "";
                    cityMsg3.css('display', 'none');
                    finalOutput[0].Address_Info3.Address3_City = $('#Vendor_Address3_City').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Address_Fax":

                if ($('#Vendor_Address_Fax').val() !== "" || $('#Vendor_Address_Fax').val() !== null) {
                    finalOutput[0].Address_Info1.Address1_FaxNumber = $('#Vendor_Address_Fax').val();
                }

                break;
            case "Vendor_Address2_Fax":
                if ($('#Vendor_Address2_Fax').val() !== "" || $('#Vendor_Address2_Fax').val() !== null) {
                    finalOutput[0].Address_Info2.Address2_FaxNumber = $('#Vendor_Address2_Fax').val();
                }

                break;
            case "Vendor_Address3_Fax":
                if ($('#Vendor_Address3_Fax').val() !== "" || $('#Vendor_Address3_Fax').val() !== null) {
                    finalOutput[0].Address_Info3.Address3_FaxNumber = $('#Vendor_Address3_Fax').val();
                }
                break;
            case "Vendor_Address_Mobile":

                var mobileMsg = $("#Vendor_Address_Mobile_ErrorMsg");
                var errorMsgMobile = "";
                mobileMsg.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address_Mobile').val() === "" || $('#Vendor_Address_Mobile').val() === null) {
                    errorMsgMobile = "Please enter mobile number";
                    mobileMsg.append(errorMsgMobile);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgMobile = "";
                    mobileMsg.css('display', 'none');
                    finalOutput[0].Address_Info1.Address1_MobileNumber = $('#Vendor_Address_Mobile').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Address2_Mobile":

                var mobileMsg2 = $("#Vendor_Address2_Mobile_ErrorMsg");
                var errorMsgMobile2 = "";
                mobileMsg2.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address2_Mobile').val() === "" || $('#Vendor_Address2_Mobile').val() === null) {
                    errorMsgMobile2 = "Please enter mobile number";
                    mobileMsg2.append(errorMsgMobile2);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgMobile2 = "";
                    mobileMsg2.css('display', 'none');
                    finalOutput[0].Address_Info2.Address2_MobileNumber = $('#Vendor_Address2_Mobile').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Address3_Mobile":

                var mobileMsg3 = $("#Vendor_Address3_Mobile_ErrorMsg");
                var errorMsgMobile3 = "";
                mobileMsg3.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Address3_Mobile').val() === "" || $('#Vendor_Address3_Mobile').val() === null) {
                    errorMsgMobile3 = "Please enter mobile number";
                    mobileMsg3.append(errorMsgMobile3);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgMobile3 = "";
                    mobileMsg3.css('display', 'none');
                    finalOutput[0].Address_Info3.Address3_MobileNumber = $('#Vendor_Address3_Mobile').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Address_Telephone":
                if ($('#Vendor_Address_Telephone').val() !== "" || $('#Vendor_Address_Telephone').val() !== null) {
                    finalOutput[0].Address_Info1.Address1_Telephone = $('#Vendor_Address_Telephone').val();
                }
                break;
            case "Vendor_Address2_Telephone":
                if ($('#Vendor_Address2_Telephone').val() !== "" || $('#Vendor_Address2_Telephone').val() !== null) {
                    finalOutput[0].Address_Info2.Address2_Telephone = $('#Vendor_Address2_Telephone').val();
                }
                break;
            case "Vendor_Address3_Telephone":
                if ($('#Vendor_Address3_Telephone').val() !== "" || $('#Vendor_Address3_Telephone').val() !== null) {
                    finalOutput[0].Address_Info3.Address3_Telephone = $('#Vendor_Address3_Telephone').val();
                }
                break;
            case "Vendor_Address_Email":

                var emailMsg = $("#Vendor_Address_Email_ErrorMsg");
                var errorMsgEmail = "";

                var emailaddress = $(this).val();

                emailMsg.html('<div class="vendorError" style="display: block;"></div>');

                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if (!emailReg.test(emailaddress)) {
                    errorMsgEmail = "Please enter valid email id";
                    emailMsg.append(errorMsgEmail);
                    e.preventDefault();
                    return false;
                }

                if ($('#Vendor_Address_Email').val() === "" || $('#Vendor_Address_Email').val() === null) {
                    errorMsgEmail = "Please enter email-id";
                    emailMsg.append(errorMsgEmail);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgEmail = "";
                    emailMsg.css('display', 'none');
                    finalOutput[0].Address_Info1.Address1_EmailID = $('#Vendor_Address_Email').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Address2_Email":

                var emailMsg2 = $("#Vendor_Address2_Email_ErrorMsg");
                var errorMsgEmail2 = "";
                emailMsg2.html('<div class="vendorError" style="display: block;"></div>');

                var emailaddress2 = $(this).val();
                var buttonNext2 = $("#address2NextBtn");

                var emailReg2 = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if (!emailReg2.test(emailaddress2)) {
                    errorMsgEmail = "Please enter valid email id";
                    emailMsg.append(errorMsgEmail);
                    buttonNext2.css('display', 'none');
                    e.preventDefault();
                    return false;
                }
                else {
                    buttonNext2.css('display', 'inline-block');
                }

                if ($('#Vendor_Address2_Email').val() === "" || $('#Vendor_Address2_Email').val() === null) {
                    errorMsgEmail2 = "Please enter email-id";
                    emailMsg2.append(errorMsgEmail2);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgEmail2 = "";
                    emailMsg2.css('display', 'none');
                    finalOutput[0].Address_Info2.Address2_EmailID = $('#Vendor_Address2_Email').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Address3_Email":

                var emailMsg3 = $("#Vendor_Address3_Email_ErrorMsg");
                var errorMsgEmail3 = "";
                emailMsg3.html('<div class="vendorError" style="display: block;"></div>');

                var emailaddress3 = $(this).val();
                var buttonNext3 = $("#address2NextBtn");

                var emailReg3 = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if (!emailReg3.test(emailaddress3)) {
                    errorMsgEmail = "Please enter valid email id";
                    emailMsg.append(errorMsgEmail);
                    buttonNext3.css('display', 'none');
                    e.preventDefault();
                    return false;
                }
                else {
                    buttonNext3.css('display', 'inline-block');
                }

                if ($('#Vendor_Address3_Email').val() === "" || $('#Vendor_Address3_Email').val() === null) {
                    errorMsgEmail3 = "Please enter email-id";
                    emailMsg3.append(errorMsgEmail3);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgEmail3 = "";
                    emailMsg3.css('display', 'none');
                    finalOutput[0].Address_Info3.Address3_EmailID = $('#Vendor_Address3_Email').val();
                    e.preventDefault();
                }

                break;
        }

    });

    $('#Vendor_Address_Form1').submit(function (e) {

        var title = $("#Vendor_Address_TitleDDL_ErrorMsg");
        var errorMsgtitle = "";
        title.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address_TitleDDL').val() === "" || $('#Vendor_Address_TitleDDL').val() === null) {
            errorMsgtitle = "Please select title";
            title.append(errorMsgtitle);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgtitle = "";
            title.css('display', 'none');
            finalOutput[0].Address_Info1.Address1_Title = $('#Vendor_Address_TitleDDL').val();
            e.preventDefault();
        }

        var countryMsg = $("#Vendor_Address_CountryDDL_ErrorMsg");
        var errorMsgCountry = "";
        countryMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address_CountryDDL').val() === "" || $('#Vendor_Address_CountryDDL').val() === null) {
            errorMsgCountry = "Please select country";
            countryMsg.append(errorMsgCountry);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgtitle = "";
            countryMsg.css('display', 'none');
            finalOutput[0].Address_Info1.Address1_Country = $('#Vendor_Address_CountryDDL').val();
            e.preventDefault();
        }

        var Name1Msg = $("#Vendor_Address_Name1_ErrorMsg");
        var errorMsgName1 = "";
        Name1Msg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address_Name1').val() === "" || $('#Vendor_Address_Name1').val() === null) {
            errorMsgName1 = "Please enter name";
            Name1Msg.append(errorMsgName1);
            e.preventDefault();
            //return false;
        }
        else {
            var name1 = $('#Vendor_Address_Name1').val();

            let subStr = new RegExp('.{1,' + 35 + '}', 'g');
            var splitName1 = name1.match(subStr);
            var finalName1 = splitName1[0];
            var finalName2 = splitName1[1];
            var finalName3 = "";

            for (var i = 2; i < splitName1.length; i++) {
                finalName3.concat(splitName1[i]);
            }

            errorMsgName1 = "";
            Name1Msg.css('display', 'none');
            finalOutput[0].Address_Info1.Address1_Name1 = finalName1;
            finalOutput[0].Address_Info1.Address1_Name2 = finalName2;
            finalOutput[0].Address_Info1.Address1_Name3 = finalName3;
            e.preventDefault();
        }

        var addressLine1Msg = $("#Vendor_Address_Line1_ErrorMsg");
        var errorMsgAddressLine1 = "";
        addressLine1Msg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address_Line1').val() === "" || $('#Vendor_Address_Line1').val() === null) {
            errorMsgAddressLine1 = "Please enter address";
            addressLine1Msg.append(errorMsgAddressLine1);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgAddressLine1 = "";
            addressLine1Msg.css('display', 'none');
            finalOutput[0].Address_Info1.Address1_AddressLine1 = $('#Vendor_Address_Line1').val();
            e.preventDefault();
        }

        var postalCodeMsg = $("#Vendor_Address_Postal_ErrorMsg");
        var errorMsgPostalCode = "";
        postalCodeMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address_Postal').val() === "" || $('#Vendor_Address_Postal').val() === null) {
            errorMsgPostalCode = "Please enter postal code";
            postalCodeMsg.append(errorMsgPostalCode);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgPostalCode = "";
            addressLine1Msg.css('display', 'none');
            finalOutput[0].Address_Info1.Address1_PostalCode = $('#Vendor_Address_Postal').val();
            e.preventDefault();
        }

        var cityMsg = $("#Vendor_Address_City_ErrorMsg");
        var errorMsgCity = "";
        cityMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address_City').val() === "" || $('#Vendor_Address_City').val() === null) {
            errorMsgCity = "Please enter city";
            cityMsg.append(errorMsgCity);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgCity = "";
            cityMsg.css('display', 'none');
            finalOutput[0].Address_Info1.Address1_City = $('#Vendor_Address_City').val();
            e.preventDefault();
        }

        var mobileMsg = $("#Vendor_Address_Mobile_ErrorMsg");
        var errorMsgMobile = "";
        mobileMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address_Mobile').val() === "" || $('#Vendor_Address_Mobile').val() === null) {
            errorMsgMobile = "Please enter mobile number";
            mobileMsg.append(errorMsgMobile);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgMobile = "";
            mobileMsg.css('display', 'none');
            finalOutput[0].Address_Info1.Address1_MobileNumber = $('#Vendor_Address_Mobile').val();
            e.preventDefault();
        }

        var emailMsg = $("#Vendor_Address_Email_ErrorMsg");
        var errorMsgEmail = "";
        emailMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address_Email').val() === "" || $('#Vendor_Address_Email').val() === null) {
            errorMsgEmail = "Please enter email-id";
            emailMsg.append(errorMsgEmail);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgEmail = "";
            emailMsg.css('display', 'none');
            finalOutput[0].Address_Info1.Address1_EmailID = $('#Vendor_Address_Email').val();
            e.preventDefault();
        }

        if ($('#Vendor_Address_TitleDDL').val() !== "" && $('#Vendor_Address_TitleDDL').val() !== null && $('#Vendor_Address_Name1').val() !== "" && $('#Vendor_Address_Name1').val() !== null &&
            $('#Vendor_Address_CountryDDL').val() !== "" && $('#Vendor_Address_CountryDDL').val() !== null && $('#Vendor_Address_Line1').val() !== "" && $('#Vendor_Address_Line1').val() !== null &&
            $('#Vendor_Address_Postal').val() !== "" && $('#Vendor_Address_Postal').val() !== null && $('#Vendor_Address_City').val() !== "" && $('#Vendor_Address_City').val() !== null &&
            $('#Vendor_Address_Mobile').val() !== "" && $('#Vendor_Address_Mobile').val() !== null && $('#Vendor_Address_Email').val() !== "" && $('#Vendor_Address_Email').val() !== null) {

            //finalOutput[0].Company_Info.CompanyCode = Number.parseInt($('#Vendor_Profile_CompanyCodeDDL').val());
            //finalOutput[0].Company_Info.CompanyCode = $('#Vendor_Profile_PurchaseOrgDDL').val();

            $("#vendor_AddressInfo").removeClass('show active');
            $("#vendor_BankInfo").addClass('show active');
            $("#vendor_Bank_Tab").addClass('active');
            $("#vendor_Address_Tab").css("pointer-events", "auto");
            $("#vendor_Bank_Tab").css("pointer-events", "auto");

            e.preventDefault();
        }

        event.preventDefault();
    });

    $('#Vendor_Address2_Form').submit(function (e) {

        var title = $("#Vendor_Address2_TitleDDL_ErrorMsg");
        var errorMsgtitle = "";
        title.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address2_TitleDDL').val() === "" || $('#Vendor_Address2_TitleDDL').val() === null) {
            errorMsgtitle = "Please select title";
            title.append(errorMsgtitle);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgtitle = "";
            title.css('display', 'none');
            finalOutput[0].Address_Info2.Address2_Title = $('#Vendor_Address2_TitleDDL').val();
            e.preventDefault();
        }

        var countryMsg = $("#Vendor_Address2_CountryDDL_ErrorMsg");
        var errorMsgCountry = "";
        countryMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address2_CountryDDL').val() === "" || $('#Vendor_Address2_CountryDDL').val() === null) {
            errorMsgCountry = "Please select country";
            countryMsg.append(errorMsgCountry);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgtitle = "";
            countryMsg.css('display', 'none');
            finalOutput[0].Address_Info2.Address2_Country = $('#Vendor_Address2_CountryDDL').val();
            e.preventDefault();
        }

        var Name1Msg = $("#Vendor_Address2_Name1_ErrorMsg");
        var errorMsgName1 = "";
        Name1Msg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address2_Name1').val() === "" || $('#Vendor_Address2_Name1').val() === null) {
            errorMsgName1 = "Please enter name";
            Name1Msg.append(errorMsgName1);
            e.preventDefault();
            //return false;
        }
        else {
            var name1 = $('#Vendor_Address2_Name1').val();

            let subStr = new RegExp('.{1,' + 35 + '}', 'g');
            var splitName1 = name1.match(subStr);
            var finalName1 = splitName1[0];
            var finalName2 = splitName1[1];
            var finalName3 = "";

            for (var i = 2; i < splitName1.length; i++) {
                finalName3.concat(splitName1[i]);
            }

            errorMsgName1 = "";
            Name1Msg.css('display', 'none');
            finalOutput[0].Address_Info2.Address2_Name1 = finalName1;
            finalOutput[0].Address_Info2.Address2_Name2 = finalName2;
            finalOutput[0].Address_Info2.Address2_Name3 = finalName3;
            e.preventDefault();
        }

        var addressLine1Msg = $("#Vendor_Address2_Line1_ErrorMsg");
        var errorMsgAddressLine1 = "";
        addressLine1Msg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address2_Line1').val() === "" || $('#Vendor_Address2_Line1').val() === null) {
            errorMsgAddressLine1 = "Please enter address";
            addressLine1Msg.append(errorMsgAddressLine1);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgAddressLine1 = "";
            addressLine1Msg.css('display', 'none');
            finalOutput[0].Address_Info2.Address2_AddressLine1 = $('#Vendor_Address2_Line1').val();
            e.preventDefault();
        }

        var postalCodeMsg = $("#Vendor_Address2_Postal_ErrorMsg");
        var errorMsgPostalCode = "";
        postalCodeMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address2_Postal').val() === "" || $('#Vendor_Address2_Postal').val() === null) {
            errorMsgPostalCode = "Please enter postal code";
            postalCodeMsg.append(errorMsgPostalCode);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgPostalCode = "";
            postalCodeMsg.css('display', 'none');
            finalOutput[0].Address_Info2.Address2_PostalCode = $('#Vendor_Address2_Postal').val();
            e.preventDefault();
        }

        var cityMsg = $("#Vendor_Address2_City_ErrorMsg");
        var errorMsgCity = "";
        cityMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address2_City').val() === "" || $('#Vendor_Address2_City').val() === null) {
            errorMsgCity = "Please enter city";
            cityMsg.append(errorMsgCity);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgCity = "";
            cityMsg.css('display', 'none');
            finalOutput[0].Address_Info2.Address2_City = $('#Vendor_Address2_City').val();
            e.preventDefault();
        }

        var mobileMsg = $("#Vendor_Address2_Mobile_ErrorMsg");
        var errorMsgMobile = "";
        mobileMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address2_Mobile').val() === "" || $('#Vendor_Address2_Mobile').val() === null) {
            errorMsgMobile = "Please enter mobile number";
            mobileMsg.append(errorMsgMobile);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgMobile = "";
            mobileMsg.css('display', 'none');
            finalOutput[0].Address_Info2.Address2_MobileNumber = $('#Vendor_Address2_Mobile').val();
            e.preventDefault();
        }

        var emailMsg = $("#Vendor_Address2_Email_ErrorMsg");
        var errorMsgEmail = "";
        emailMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address2_Email').val() === "" || $('#Vendor_Address2_Email').val() === null) {
            errorMsgEmail = "Please enter email-id";
            emailMsg.append(errorMsgEmail);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgEmail = "";
            emailMsg.css('display', 'none');
            finalOutput[0].Address_Info2.Address2_EmailID = $('#Vendor_Address2_Email').val();
            e.preventDefault();
        }

        if ($('#Vendor_Address2_TitleDDL').val() !== "" && $('#Vendor_Address2_TitleDDL').val() !== null && $('#Vendor_Address2_Name1').val() !== "" && $('#Vendor_Address2_Name1').val() !== null &&
            $('#Vendor_Address2_Line1').val() !== "" && $('#Vendor_Address2_Line1').val() !== null && $('#Vendor_Address2_Postal').val() !== "" && $('#Vendor_Address2_Postal').val() !== null &&
            $('#Vendor_Address2_City').val() !== "" && $('#Vendor_Address2_City').val() !== null && $('#Vendor_Address2_Country').val() !== "" && $('#Vendor_Address2_Country').val() !== null &&
            $('#Vendor_Address2_Mobile').val() !== "" && $('#Vendor_Address2_Mobile').val() !== null && $('#Vendor_Address2_Email').val() !== "" && $('#Vendor_Address2_Email').val() !== null) {

            //finalOutput[0].Company_Info.CompanyCode = Number.parseInt($('#Vendor_Profile_CompanyCodeDDL').val());
            //finalOutput[0].Company_Info.CompanyCode = $('#Vendor_Profile_PurchaseOrgDDL').val();

            $("#vendor_AddressInfo").removeClass('show active');
            $("#vendor_BankInfo").addClass('show active');
            $("#vendor_Bank_Tab").addClass('active');
            $("#vendor_Address_Tab").css("pointer-events", "auto");
            $("#vendor_Bank_Tab").css("pointer-events", "auto");

            e.preventDefault();
        }

        event.preventDefault();
    });

    $('#Vendor_Address3_Form').submit(function (e) {

        var title = $("#Vendor_Address3_TitleDDL_ErrorMsg");
        var errorMsgtitle = "";
        title.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address3_TitleDDL').val() === "" || $('#Vendor_Address3_TitleDDL').val() === null) {
            errorMsgtitle = "Please select title";
            title.append(errorMsgtitle);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgtitle = "";
            title.css('display', 'none');
            finalOutput[0].Address_Info3.Address3_Title = $('#Vendor_Address3_TitleDDL').val();
            e.preventDefault();
        }

        var countryMsg = $("#Vendor_Address3_CountryDDL_ErrorMsg");
        var errorMsgCountry = "";
        countryMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address3_CountryDDL').val() === "" || $('#Vendor_Address3_CountryDDL').val() === null) {
            errorMsgCountry = "Please select country";
            countryMsg.append(errorMsgCountry);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgtitle = "";
            countryMsg.css('display', 'none');
            finalOutput[0].Address_Info3.Address3_Country = $('#Vendor_Address3_CountryDDL').val()
            e.preventDefault();
        }

        var Name1Msg = $("#Vendor_Address3_Name1_ErrorMsg");
        var errorMsgName1 = "";
        Name1Msg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address3_Name1').val() === "" || $('#Vendor_Address3_Name1').val() === null) {
            errorMsgName1 = "Please enter name";
            Name1Msg.append(errorMsgName1);
            e.preventDefault();
            //return false;
        }
        else {
            var name1 = $('#Vendor_Address3_Name1').val();

            let subStr = new RegExp('.{1,' + 35 + '}', 'g');
            var splitName1 = name1.match(subStr);
            var finalName1 = splitName1[0];
            var finalName2 = splitName1[1];
            var finalName3 = "";

            for (var i = 2; i < splitName1.length; i++) {
                finalName3.concat(splitName1[i]);
            }

            errorMsgName1 = "";
            Name1Msg.css('display', 'none');
            finalOutput[0].Address_Info3.Address3_Name1 = finalName1;
            finalOutput[0].Address_Info3.Address3_Name2 = finalName2;
            finalOutput[0].Address_Info3.Address3_Name3 = finalName3;
            e.preventDefault();
        }

        var addressLine1Msg = $("#Vendor_Address3_Line1_ErrorMsg");
        var errorMsgAddressLine1 = "";
        addressLine1Msg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address3_Line1').val() === "" || $('#Vendor_Address3_Line1').val() === null) {
            errorMsgAddressLine1 = "Please enter address";
            addressLine1Msg.append(errorMsgAddressLine1);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgAddressLine1 = "";
            addressLine1Msg.css('display', 'none');
            finalOutput[0].Address_Info3.Address3_AddressLine1 = $('#Vendor_Address3_Line1').val();
            e.preventDefault();
        }

        var postalCodeMsg = $("#Vendor_Address3_Postal_ErrorMsg");
        var errorMsgPostalCode = "";
        postalCodeMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address3_Postal').val() === "" || $('#Vendor_Address3_Postal').val() === null) {
            errorMsgPostalCode = "Please enter postal code";
            postalCodeMsg.append(errorMsgPostalCode);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgPostalCode = "";
            postalCodeMsg.css('display', 'none');
            finalOutput[0].Address_Info3.Address3_PostalCode = $('#Vendor_Address3_Postal').val();
            e.preventDefault();
        }

        var cityMsg = $("#Vendor_Address3_City_ErrorMsg");
        var errorMsgCity = "";
        cityMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address3_City').val() === "" || $('#Vendor_Address3_City').val() === null) {
            errorMsgCity = "Please enter city";
            cityMsg.append(errorMsgCity);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgCity = "";
            cityMsg.css('display', 'none');
            finalOutput[0].Address_Info3.Address3_City = $('#Vendor_Address3_City').val();
            e.preventDefault();
        }

        var mobileMsg = $("#Vendor_Address3_Mobile_ErrorMsg");
        var errorMsgMobile = "";
        mobileMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address3_Mobile').val() === "" || $('#Vendor_Address3_Mobile').val() === null) {
            errorMsgMobile = "Please enter mobile number";
            mobileMsg.append(errorMsgMobile);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgMobile = "";
            mobileMsg.css('display', 'none');
            finalOutput[0].Address_Info3.Address3_MobileNumber = $('#Vendor_Address3_Mobile').val();
            e.preventDefault();
        }

        var emailMsg = $("#Vendor_Address3_Email_ErrorMsg");
        var errorMsgEmail = "";
        emailMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Address3_Email').val() === "" || $('#Vendor_Address3_Email').val() === null) {
            errorMsgEmail = "Please enter email-id";
            emailMsg.append(errorMsgEmail);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgEmail = "";
            emailMsg.css('display', 'none');
            finalOutput[0].Address_Info3.Address3_EmailID = $('#Vendor_Address3_Email').val();
            e.preventDefault();
        }

        if ($('#Vendor_Address3_TitleDDL').val() !== "" && $('#Vendor_Address3_TitleDDL').val() !== null && $('#Vendor_Address3_Name1').val() !== "" && $('#Vendor_Address3_Name1').val() !== null &&
            $('#Vendor_Address3_Line1').val() !== "" && $('#Vendor_Address3_Line1').val() !== null && $('#Vendor_Address3_Postal').val() !== "" && $('#Vendor_Address3_Postal').val() !== null &&
            $('#Vendor_Address3_City').val() !== "" && $('#Vendor_Address3_City').val() !== null && $('#Vendor_Address3_CountryDDL').val() !== "" && $('#Vendor_Address3_CountryDDL').val() !== null &&
            $('#Vendor_Address3_Mobile').val() !== "" && $('#Vendor_Address3_Mobile').val() !== null && $('#Vendor_Address3_Email').val() !== "" && $('#Vendor_Address3_Email').val() !== null) {

            //finalOutput[0].Company_Info.CompanyCode = Number.parseInt($('#Vendor_Profile_CompanyCodeDDL').val());
            //finalOutput[0].Company_Info.CompanyCode = $('#Vendor_Profile_PurchaseOrgDDL').val();

            $("#vendor_AddressInfo").removeClass('show active');
            $("#vendor_BankInfo").addClass('show active');
            $("#vendor_Bank_Tab").addClass('active');
            $("#vendor_Address_Tab").css("pointer-events", "auto");
            $("#vendor_Bank_Tab").css("pointer-events", "auto");

            e.preventDefault();
        }

        event.preventDefault();
    });

    ///////////////////////  Address Info Validations   ///////////////////

    $('#Vendor_Address_Name1,#Vendor_Address2_Name1,#Vendor_Address3_Name1').keypress(function (e) {

        var id = $(this)[0].id;

        var title = "";
        var errorMsgtitle = "";

        if (id === "Vendor_Address_Name1") {
            title = $("#Vendor_Address_Name1_ErrorMsg");
        }

        if (id === "Vendor_Address2_Name1") {
            title = $("#Vendor_Address2_Name1_ErrorMsg");
        }

        if (id === "Vendor_Address3_Name1") {
            title = $("#Vendor_Address3_Name1_ErrorMsg");
        }

        title.html('<div class="vendorError" style="display: block;"></div>');

        var k = e.keyCode,
            $return = ((k > 64 && k < 91) || (k > 96 && k < 123) || k === 8 || k === 32 || (k >= 48 && k <= 57));

        if (!$return) {
            errorMsgtitle = "Special characters not allowed";
            title.append(errorMsgtitle);
            e.preventDefault();
            return false;
        }

    });

    ///////////////////////////////////////////////////  Bank Info Screen  //////////////////////////////////////////////////////////////////////////////

    function loadBankCountry(countries, selectedCountry, update = "") {
        var html_code = '';

        html_code += '<option value="" disabled selected>Select Country</option>';

        $.each(countries, function (key, value) {

            if (value.country !== "" || value.country !== null) {
                if (update === "update" && selectedCountry !== null && selectedCountry !== "" && selectedCountry === value.country) {
                    html_code += '<option selected value="' + value.country + '">' + value.data + '</option>';
                }
                else {
                    html_code += '<option value="' + value.country + '">' + value.data + '</option>';
                }
            }

        });
    }

    $('#BankPreviousBtn1,#BankPreviousBtn2,#BankPreviousBtn3').click(function () {
        $('#vendor_BankInfo').removeClass('show active');
        $('#vendor_AddressInfo').addClass('show active');
    });

    $('#vendor_Bank_Add1').click(function () {
        $('#vendor_Bank_Accordion2').css('display', 'block');
        $('#vendor_Bank_Add1').css('display', 'none');
        $('#bankcollapseTwo').addClass('show');
    });

    $('#vendor_Bank_Add2').click(function () {
        $('#vendor_Bank_Accordion3').css('display', 'block');
        $('#vendor_Bank_Add2').css('display', 'none');
        $('#bankcollapseThree').addClass('show');
    });

    $('#vendor_Bank_Delete2').click(function () {
        //$('#bankcollapseTwo').removeClass('show');
        $('#vendor_Bank_Accordion2').css('display', 'none');
        $('#Vendor_Bank2_Form')[0].reset();
        $('#vendor_Bank_Add1').css('display', 'inline-block');
        $('#bankcollapseOne').addClass('show');

        $('#Vendor_Bank2_CountryDDL_ErrorMsg,#Vendor_Bank2_NameDDL_ErrorMsg,#Vendor_Bank2_SwiftDDL_ErrorMsg,#Vendor_Bank2_Account_ErrorMsg,#Vendor_Bank2_Holder_ErrorMsg,#Vendor_Bank2_IBAN_ErrorMsg').text("");
    });

    $('#vendor_Bank_Delete3').click(function () {
        //$('#bankcollapseThree').removeClass('show');
        $('#vendor_Bank_Accordion3').css('display', 'none');
        $('#Vendor_Bank3_Form')[0].reset();
        $('#vendor_Bank_Add2').css('display', 'inline-block');
        $('#bankcollapseOne').addClass('show');

        $('#Vendor_Bank3_CountryDDL_ErrorMsg,#Vendor_Bank3_NameDDL_ErrorMsg,#Vendor_Bank3_SwiftDDL_ErrorMsg,#Vendor_Bank3_Account_ErrorMsg,#Vendor_Bank3_Holder_ErrorMsg,#Vendor_Bank3_IBAN_ErrorMsg').text("");
    });

    $('#Vendor_Bank_Form1').submit(function (e) {

        var countryMsg = $("#Vendor_Bank_CountryDDL_ErrorMsg");
        var errorMsgCountry = "";
        countryMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank_CountryDDL').val() === "" || $('#Vendor_Bank_CountryDDL').val() === null) {
            errorMsgCountry = "Please select country";
            countryMsg.append(errorMsgCountry);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgCountry = "";
            countryMsg.css('display', 'none');
            finalOutput[0].Bank_Info1.Bank1_Country = $('#Vendor_Bank_CountryDDL').val();
            e.preventDefault();
        }

        var bankNameMsg1 = $("#Vendor_Bank_NameDDL_ErrorMsg");
        var errorMsgBankName1 = "";
        bankNameMsg1.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank_NameDDL').val() === "" || $('#Vendor_Bank_NameDDL').val() === null) {
            errorMsgBankName1 = "Please select bank name";
            bankNameMsg1.append(errorMsgBankName1);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankName1 = "";
            bankNameMsg1.css('display', 'none');
            finalOutput[0].Bank_Info1.Bank1_BankName = $('#Vendor_Bank_NameDDL').val();
            e.preventDefault();
        }

        var bankKey1Msg = $("#Vendor_Bank_SwiftDDL_ErrorMsg");
        var errorMsgBankKey1 = "";
        bankKey1Msg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank_SwiftDDL').val() === "" || $('#Vendor_Bank_SwiftDDL').val() === null) {
            errorMsgBankKey1 = "Please select bank key/swift code";
            bankKey1Msg.append(errorMsgBankKey1);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankKey1 = "";
            bankKey1Msg.css('display', 'none');
            finalOutput[0].Bank_Info1.Bank1_SwiftCode = $('#Vendor_Bank_SwiftDDL').val();
            e.preventDefault();
        }

        var bankAccountMsg1 = $("#Vendor_Bank_Account_ErrorMsg");
        var errorMsgBankAccount = "";
        bankAccountMsg1.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank_Account').val() === "" || $('#Vendor_Bank_Account').val() === null) {
            errorMsgBankAccount = "Please enter bank account number";
            bankAccountMsg1.append(errorMsgBankAccount);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankAccount = "";
            bankAccountMsg1.css('display', 'none');
            finalOutput[0].Bank_Info1.Bank1_AccountNumber = $('#Vendor_Bank_Account').val();
            e.preventDefault();
        }

        var bankHolderMsg1 = $("#Vendor_Bank_Holder_ErrorMsg");
        var errorMsgBankHolder = "";
        bankHolderMsg1.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank_Holder').val() === "" || $('#Vendor_Bank_Holder').val() === null) {
            errorMsgBankHolder = "Please enter bank holder name";
            bankHolderMsg1.append(errorMsgBankHolder);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankHolder = "";
            bankHolderMsg1.css('display', 'none');
            finalOutput[0].Bank_Info1.Bank1_AccountHolderName = $('#Vendor_Bank_Holder').val();
            e.preventDefault();
        }

        var ibanMsg1 = $("#Vendor_Bank_IBAN_ErrorMsg");
        var errorMsgIban1 = "";
        ibanMsg1.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank_IBAN').val() === "" || $('#Vendor_Bank_IBAN').val() === null) {
            errorMsgIban1 = "Please enter IBAN value";
            ibanMsg1.append(errorMsgIban1);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgIban1 = "";
            ibanMsg1.css('display', 'none');
            finalOutput[0].Bank_Info1.Bank1_IbanValue = $('#Vendor_Bank_IBAN').val();
            e.preventDefault();
        }

        if ($('#Vendor_Bank_CountryDDL').val() !== "" && $('#Vendor_Bank_CountryDDL').val() !== null && $('#Vendor_Bank_NameDDL').val() !== "" && $('#Vendor_Bank_NameDDL').val() !== null &&
            $('#Vendor_Bank_SwiftDDL').val() !== "" && $('#Vendor_Bank_SwiftDDL').val() !== null && $('#Vendor_Bank_Account').val() !== "" && $('#Vendor_Bank_Account').val() !== null &&
            $('#Vendor_Bank_Holder').val() !== "" && $('#Vendor_Bank_Holder').val() !== null && $('#Vendor_Bank_IBAN').val() !== "" && $('#Vendor_Bank_IBAN').val() !== null) {

            //finalOutput[0].Company_Info.CompanyCode = Number.parseInt($('#Vendor_Profile_CompanyCodeDDL').val());
            //finalOutput[0].Company_Info.CompanyCode = $('#Vendor_Profile_PurchaseOrgDDL').val();

            $("#vendor_BankInfo").removeClass('show active');
            $("#vendor_TaxInfo").addClass('show active');
            $("#vendor_Tax_Tab").addClass('active');
            //$("#vendor_Bank_Tab").css("pointer-events", "auto");
            $("#vendor_Tax_Tab").css("pointer-events", "auto");

            e.preventDefault();
        }

        event.preventDefault();
    });

    $('#Vendor_Bank2_Form').submit(function (e) {

        var countryMsg2 = $("#Vendor_Bank2_CountryDDL_ErrorMsg");
        var errorMsgCountry2 = "";
        countryMsg2.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank2_CountryDDL').val() === "" || $('#Vendor_Bank2_CountryDDL').val() === null) {
            errorMsgCountry2 = "Please select country";
            countryMsg2.append(errorMsgCountry2);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgCountry2 = "";
            countryMsg2.css('display', 'none');
            finalOutput[0].Bank_Info2.Bank2_Country = $('#Vendor_Bank2_CountryDDL').val();
            e.preventDefault();
        }

        var bankNameMsg2 = $("#Vendor_Bank2_NameDDL_ErrorMsg");
        var errorMsgBankName2 = "";
        bankNameMsg2.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank2_NameDDL').val() === "" || $('#Vendor_Bank2_NameDDL').val() === null) {
            errorMsgBankName2 = "Please select bank name";
            bankNameMsg2.append(errorMsgBankName2);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankName2 = "";
            bankNameMsg2.css('display', 'none');
            finalOutput[0].Bank_Info2.Bank2_BankName = $('#Vendor_Bank2_NameDDL').val();
            e.preventDefault();
        }

        var bankKey1Msg2 = $("#Vendor_Bank2SwiftDDL_ErrorMsg");
        var errorMsgBankKey2 = "";
        bankKey1Msg2.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank2_SwiftDDL').val() === "" || $('#Vendor_Bank2_SwiftDDL').val() === null) {
            errorMsgBankKey2 = "Please select bank key/swift code";
            bankKey1Msg2.append(errorMsgBankKey2);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankKey2 = "";
            bankKey1Msg2.css('display', 'none');
            finalOutput[0].Bank_Info2.Bank2_SwiftCode = $('#Vendor_Bank2_SwiftDDL').val();
            e.preventDefault();
        }

        var bankAccountMsg2 = $("#Vendor_Bank2_Account_ErrorMsg");
        var errorMsgBankAccount2 = "";
        bankAccountMsg2.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank2_Account').val() === "" || $('#Vendor_Bank2_Account').val() === null) {
            errorMsgBankAccount2 = "Please enter bank account number";
            bankAccountMsg2.append(errorMsgBankAccount2);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankAccount2 = "";
            bankAccountMsg2.css('display', 'none');
            finalOutput[0].Bank_Info2.Bank2_AccountNumber = $('#Vendor_Bank2_Account').val();
            e.preventDefault();
        }

        var bankHolderMsg2 = $("#Vendor_Bank2_Holder_ErrorMsg");
        var errorMsgBankHolder2 = "";
        bankHolderMsg2.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank2_Holder').val() === "" || $('#Vendor_Bank2_Holder').val() === null) {
            errorMsgBankHolder2 = "Please enter bank holder name";
            bankHolderMsg2.append(errorMsgBankHolder2);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankHolder = "";
            bankHolderMsg2.css('display', 'none');
            finalOutput[0].Bank_Info2.Bank2_AccountHolderName = $('#Vendor_Bank2_Holder').val();
            e.preventDefault();
        }

        var ibanMsg2 = $("#Vendor_Bank2_IBAN_ErrorMsg");
        var errorMsgIban2 = "";
        ibanMsg2.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank2_IBAN').val() === "" || $('#Vendor_Bank2_IBAN').val() === null) {
            errorMsgIban2 = "Please enter IBAN value";
            ibanMsg2.append(errorMsgIban2);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgIban2 = "";
            ibanMsg2.css('display', 'none');
            finalOutput[0].Bank_Info2.Bank2_IbanValue = $('#Vendor_Bank2_IBAN').val();
            e.preventDefault();
        }

        if ($('#Vendor_Bank2_CountryDDL').val() !== "" && $('#Vendor_Bank2_CountryDDL').val() !== null && $('#Vendor_Bank2_NameDDL').val() !== "" && $('#Vendor_Bank2_NameDDL').val() !== null &&
            $('#Vendor_Bank2_SwiftDDL').val() !== "" && $('#Vendor_Bank2_SwiftDDL').val() !== null && $('#Vendor_Bank2_Account').val() !== "" && $('#Vendor_Bank2_Account').val() !== null &&
            $('#Vendor_Bank2_Holder').val() !== "" && $('#Vendor_Bank2_Holder').val() !== null && $('#Vendor_Bank2_IBAN').val() !== "" && $('#Vendor_Bank2_IBAN').val() !== null) {

            //finalOutput[0].Company_Info.CompanyCode = Number.parseInt($('#Vendor_Profile_CompanyCodeDDL').val());
            //finalOutput[0].Company_Info.CompanyCode = $('#Vendor_Profile_PurchaseOrgDDL').val();

            $("#vendor_BankInfo").removeClass('show active');
            $("#vendor_TaxInfo").addClass('show active');
            $("#vendor_Tax_Tab").addClass('active');
            //$("#vendor_Bank_Tab").css("pointer-events", "auto");
            $("#vendor_Tax_Tab").css("pointer-events", "auto");

            e.preventDefault();
        }

        event.preventDefault();
    });

    $('#Vendor_Bank3_Form').submit(function (e) {

        var countryMsg3 = $("#Vendor_Bank3_CountryDDL_ErrorMsg");
        var errorMsgCountry3 = "";
        countryMsg3.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank3_CountryDDL').val() === "" || $('#Vendor_Bank3_CountryDDL').val() === null) {
            errorMsgCountry3 = "Please select country";
            countryMsg3.append(errorMsgCountry3);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgCountry3 = "";
            countryMsg3.css('display', 'none');
            finalOutput[0].Bank_Info3.Bank3_Country = $('#Vendor_Bank3_CountryDDL').val();
            e.preventDefault();
        }

        var bankNameMsg3 = $("#Vendor_Bank3_NameDDL_ErrorMsg");
        var errorMsgBankName3 = "";
        bankNameMsg3.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank3_NameDDL').val() === "" || $('#Vendor_Bank3_NameDDL').val() === null) {
            errorMsgBankName3 = "Please select bank name";
            bankNameMsg3.append(errorMsgBankName3);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankName3 = "";
            bankNameMsg3.css('display', 'none');
            finalOutput[0].Bank_Info3.Bank3_BankName = $('#Vendor_Bank3_NameDDL').val();
            e.preventDefault();
        }

        var bankKey1Msg3 = $("#Vendor_Bank3_SwiftDDL_ErrorMsg");
        var errorMsgBankKey3 = "";
        bankKey1Msg3.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank3_SwiftDDL').val() === "" || $('#Vendor_Bank3_SwiftDDL').val() === null) {
            errorMsgBankKey3 = "Please select bank key/swift code";
            bankKey1Msg3.append(errorMsgBankKey3);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankKey3 = "";
            bankKey1Msg3.css('display', 'none');
            finalOutput[0].Bank_Info3.Bank3_SwiftCode = $('#Vendor_Bank3_SwiftDDL').val();
            e.preventDefault();
        }

        var bankAccountMsg3 = $("#Vendor_Bank3_Account_ErrorMsg");
        var errorMsgBankAccount3 = "";
        bankAccountMsg3.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank3_Account').val() === "" || $('#Vendor_Bank3_Account').val() === null) {
            errorMsgBankAccount3 = "Please enter bank account number";
            bankAccountMsg3.append(errorMsgBankAccount3);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankAccount3 = "";
            bankAccountMsg3.css('display', 'none');
            finalOutput[0].Bank_Info3.Bank3_AccountNumber = $('#Vendor_Bank3_Account').val();
            e.preventDefault();
        }

        var bankHolderMsg3 = $("#Vendor_Bank3_Holder_ErrorMsg");
        var errorMsgBankHolder3 = "";
        bankHolderMsg3.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank3_Holder').val() === "" || $('#Vendor_Bank3_Holder').val() === null) {
            errorMsgBankHolder3 = "Please enter bank holder name";
            bankHolderMsg3.append(errorMsgBankHolder3);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgBankHolder3 = "";
            bankHolderMsg3.css('display', 'none');
            finalOutput[0].Bank_Info3.Bank3_AccountHolderName = $('#Vendor_Bank3_Holder').val();
            e.preventDefault();
        }

        var ibanMsg3 = $("#Vendor_Bank3_IBAN_ErrorMsg");
        var errorMsgIban3 = "";
        ibanMsg3.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Bank3_IBAN').val() === "" || $('#Vendor_Bank3_IBAN').val() === null) {
            errorMsgIban3 = "Please enter IBAN value";
            ibanMsg3.append(errorMsgIban3);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgIban3 = "";
            ibanMsg3.css('display', 'none');
            finalOutput[0].Bank_Info3.Bank3_IbanValue = $('#Vendor_Bank3_IBAN').val();
            e.preventDefault();
        }

        if ($('#Vendor_Bank3_CountryDDL').val() !== "" && $('#Vendor_Bank3_CountryDDL').val() !== null && $('#Vendor_Bank3_NameDDL').val() !== "" && $('#Vendor_Bank3_NameDDL').val() !== null &&
            $('#Vendor_Bank3_SwiftDDL').val() !== "" && $('#Vendor_Bank3_SwiftDDL').val() !== null && $('#Vendor_Bank3_Account').val() !== "" && $('#Vendor_Bank3_Account').val() !== null &&
            $('#Vendor_Bank3_Holder').val() !== "" && $('#Vendor_Bank3_Holder').val() !== null && $('#Vendor_Bank3_IBAN').val() !== "" && $('#Vendor_Bank3_IBAN').val() !== null) {

            //finalOutput[0].Company_Info.CompanyCode = Number.parseInt($('#Vendor_Profile_CompanyCodeDDL').val());
            //finalOutput[0].Company_Info.CompanyCode = $('#Vendor_Profile_PurchaseOrgDDL').val();

            $("#vendor_BankInfo").removeClass('show active');
            $("#vendor_TaxInfo").addClass('show active');
            $("#vendor_Tax_Tab").addClass('active');
            //$("#vendor_Bank_Tab").css("pointer-events", "auto");
            $("#vendor_Tax_Tab").css("pointer-events", "auto");

            e.preventDefault();
        }

        event.preventDefault();
    });


    ///////////////////////  Bank Info Validations   ///////////////////

    $('#Vendor_Bank_Holder,#Vendor_Bank2_Holder,#Vendor_Bank3_Holder,#Vendor_Address_Name1,#Vendor_Address2_Name1,#Vendor_Address3_Name1,#Vendor_Address_City,#Vendor_Address2_City,#Vendor_Address3_City').keypress(function (e) {

        var id = $(this)[0].id;

        var title = "";
        var errorMsgtitle = "";

        if (id === "Vendor_Bank_Holder") {
            title = $("#Vendor_Bank_Holder_ErrorMsg");
        }

        if (id === "Vendor_Bank2_Holder") {
            title = $("#Vendor_Bank2_Holder_ErrorMsg");
        }

        if (id === "Vendor_Bank3_Holder") {
            title = $("#Vendor_Bank3_Holder_ErrorMsg");
        }

        if (id === "Vendor_Address_Name1") {
            title = $("#Vendor_Address_Name1_ErrorMsg");
        }

        if (id === "Vendor_Address2_Name1") {
            title = $("#Vendor_Address2_Name1_ErrorMsg");
        }

        if (id === "Vendor_Address3_Name1") {
            title = $("#Vendor_Address3_Name1_ErrorMsg");
        }

        if (id === "Vendor_Address_City") {
            title = $("#Vendor_Address_City_ErrorMsg");
        }

        if (id === "Vendor_Address2_City") {
            title = $("#Vendor_Address2_City_ErrorMsg");
        }

        if (id === "Vendor_Address3_City") {
            title = $("#Vendor_Address3_City_ErrorMsg");
        }

        title.html('<div class="vendorError" style="display: block;"></div>');

        var key = e.keyCode,
            $return = ((key > 96 && key < 123) || (key > 64 && key < 91) || (key === 32));

        if (!$return) {
            errorMsgtitle = "Only Alphabets are Allowed";
            title.append(errorMsgtitle);
            e.preventDefault();
            //return false;
        };

    });

    $('#Vendor_Bank_IBAN,#Vendor_Bank2_IBAN,#Vendor_Bank3_IBAN,#Vendor_Tax_Vat,#Vendor_Tax_Taxnum1,#Vendor_Tax_Taxnum2,#Vendor_Tax_Taxnum3').keypress(function (e) {

        var id = $(this)[0].id;

        var title = "";
        var errorMsgtitle = "";

        if (id === "Vendor_Bank_IBAN") {
            title = $("#Vendor_Bank_IBAN_ErrorMsg");
        }

        if (id === "Vendor_Bank2_IBAN") {
            title = $("#Vendor_Bank2_IBAN_ErrorMsg");
        }

        if (id === "Vendor_Bank3_IBAN") {
            title = $("#Vendor_Bank3_IBAN_ErrorMsg");
        }

        if (id === "Vendor_Tax_Vat") {
            title = $("#Vendor_Tax_Vat_ErrorMsg");
        }

        if (id === "Vendor_Tax_Taxnum1") {
            title = $("#Vendor_Tax_Taxnum1_ErrorMsg");
        }

        if (id === "Vendor_Tax_Taxnum2") {
            title = $("#Vendor_Tax_Taxnum2_ErrorMsg");
        }

        if (id === "Vendor_Tax_Taxnum3") {
            title = $("#Vendor_Tax_Taxnum3_ErrorMsg");
        }

        title.html('<div class="vendorError" style="display: block;"></div>');

        var key = e.keyCode,
            $return = ((key > 96 && key < 123) || (key > 64 && key < 91) || (key >= 48 && key <= 57));

        if (!$return) {
            errorMsgtitle = "Only AlphaNumeric are Allowed";
            title.append(errorMsgtitle);
            e.preventDefault();
            //return false;
        };

    });

    $(document).on('change', '#Vendor_Bank_CountryDDL,#Vendor_Bank2_CountryDDL,#Vendor_Bank3_CountryDDL', function () {
        var bankCountry = $(this).val();
        var id = $(this)[0].id;

        if (bankCountry !== '' && bankCountry !== null) {
            if (id === 'Vendor_Bank_CountryDDL') {
                $('#Vendor_Bank_CountryDDL_ErrorMsg').text('');
            }
            else if (id === 'Vendor_Bank2_CountryDDL') {
                $('#Vendor_Bank2_CountryDDL_ErrorMsg').text('');
            }
            else {
                $('#Vendor_Bank3_CountryDDL_ErrorMsg').text('');
            }

            BankAccountValidation(bankCountry, id);
            loadJsonBankNames(bankCountry, id, null, null);
        }
        else {
            $("#Vendor_Bank_NameDDL").html('<option value="">Select Bank Name</option>');
            $("#Vendor_Bank2_NameDDL").html('<option value="">Select Bank Name</option>');
            $("#Vendor_Bank3_NameDDL").html('<option value="">Select Bank Name</option>');
        }

    });

    function BankAccountValidation(selectedCountry,id) {
        var list = localStorage.getItem('AlfrescoCountries');
        var countriesList = $.parseJSON(list);

        $.each(countriesList, function (key, value) {
            if (value.country === selectedCountry && value.bankAccount_MaxLength !== null) {
                if (id === "Vendor_Bank_CountryDDL") {
                    $("#Vendor_Bank_Account").val('');
                    $("#Vendor_Bank_Account").attr('maxlength', value.bankAccount_MaxLength);
                }
                else if (id === "Vendor_Bank2_CountryDDL") {
                    $("#Vendor_Bank2_Account").val('');
                    $("#Vendor_Bank2_Account").attr('maxlength', value.bankAccount_MaxLength);
                }
                else {
                    $("#Vendor_Bank3_Account").val('');
                    $("#Vendor_Bank3_Account").attr('maxlength', value.bankAccount_MaxLength);
                }
            }
        });
    }

    function loadJsonBankNames(country, id, BankName = "", update = "") {
        var html_code = '';

        $.getJSON('../Json/BankNames.json', function (data) {

            html_code += '<option value="" disabled selected>Select Bank Name</option>';

            $.each(data, function (key, value) {

                if (value.Bank_Country === country) {

                    if (update === "update" && value.Bank_Country === country) {
                        html_code += '<option selected value="' + value.Bank_Name + '">' + value.Bank_Name + '</option>';
                    }
                    else {
                        html_code += '<option value="' + value.Bank_Name + '">' + value.Bank_Name + '</option>';
                    }

                }

            });

            if (id === "Vendor_Bank_CountryDDL")
                $("#Vendor_Bank_NameDDL").html(html_code);

            if (id === "Vendor_Bank2_CountryDDL")
                $("#Vendor_Bank2_NameDDL").html(html_code);

            if (id === "Vendor_Bank3_CountryDDL")
                $("#Vendor_Bank3_NameDDL").html(html_code);
        });
    }

    $(document).on('change', '#Vendor_Bank_NameDDL,#Vendor_Bank2_NameDDL,#Vendor_Bank3_NameDDL', function () {
        var bankName = $(this).val();
        var id = $(this)[0].id;

        if (bankName !== '' && bankName !== null) {
            if (id === 'Vendor_Bank_NameDDL')
                $('#Vendor_Bank_NameDDL_ErrorMsg').text('');
            else if (id === 'Vendor_Bank2_NameDDL')
                $('#Vendor_Bank2_NameDDL_ErrorMsg').text('');
            else
                $('#Vendor_Bank3_NameDDL_ErrorMsg').text('');

            loadJsonBankKeys(bankName, id, null);

        }
        else {
            $("#Vendor_Bank_SwiftDDL").html('<option value="">Select Bank Key</option>');
            $("#Vendor_Bank2_SwiftDDL").html('<option value="">Select Bank Key</option>');
            $("#Vendor_Bank3_SwiftDDL").html('<option value="">Select Bank Key</option>');
        }

    });

    function loadJsonBankKeys(bankName, id, update = "") {
        var html_code = '';

        $.getJSON('../Json/BankKeys.json', function (data) {

            html_code += '<option value="" disabled selected>Select Bank Key</option><option value="SWIFT CODE">SWIFT CODE</option>';

            $.each(data, function (key, value) {

                if (value.Bank_Name === bankName) {

                    if (update === "update") {
                        html_code += '<option selected value="' + value.Bank_Key + '">' + value.Bank_Key + '</option>';
                    }
                    else {
                        html_code += '<option value="' + value.Bank_Key + '">' + value.Bank_Key + '</option>';
                    }

                }

            });

            if (id === "Vendor_Bank_NameDDL")
                $("#Vendor_Bank_SwiftDDL").html(html_code);

            if (id === "Vendor_Bank2_NameDDL")
                $("#Vendor_Bank2_SwiftDDL").html(html_code);

            if (id === "Vendor_Bank3_NameDDL")
                $("#Vendor_Bank3_SwiftDDL").html(html_code);

        });
    }

    $('#Vendor_Bank_Account,#Vendor_Bank2_Account,#Vendor_Bank3_Account,#Vendor_Bank_Holder,#Vendor_Bank2_Holder,#Vendor_Bank3_Holder,#Vendor_Bank_IBAN,#Vendor_Bank2_IBAN,#Vendor_Bank3_IBAN').focusout(function (e) {

        var id = $(this)[0].id;

        switch (id) {
            case "Vendor_Bank_Account":

                var bankAccountMsg1 = $("#Vendor_Bank_Account_ErrorMsg");
                var errorMsgBankAccount = "";
                bankAccountMsg1.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Bank_Account').val() === "" || $('#Vendor_Bank_Account').val() === null) {
                    errorMsgBankAccount = "Please enter bank account number";
                    bankAccountMsg1.append(errorMsgBankAccount);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgBankAccount = "";
                    bankAccountMsg1.css('display', 'none');
                    finalOutput[0].Bank_Info1.Bank1_AccountNumber = $('#Vendor_Bank_Account').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Bank2_Account":

                var bankAccountMsg12 = $("#Vendor_Bank2_Account_ErrorMsg");
                var errorMsgBankAccount12 = "";
                bankAccountMsg12.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Bank2_Account').val() === "" || $('#Vendor_Bank2_Account').val() === null) {
                    errorMsgBankAccount12 = "Please enter bank account number";
                    bankAccountMsg12.append(errorMsgBankAccount12);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgBankAccount12 = "";
                    bankAccountMsg12.css('display', 'none');
                    finalOutput[0].Bank_Info2.Bank2_AccountNumber = $('#Vendor_Bank2_Account').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Bank3_Account":

                var bankAccountMsg123 = $("#Vendor_Bank3_Account_ErrorMsg");
                var errorMsgBankAccount123 = "";
                bankAccountMsg123.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Bank3_Account').val() === "" || $('#Vendor_Bank3_Account').val() === null) {
                    errorMsgBankAccount123 = "Please enter bank account number";
                    bankAccountMsg123.append(errorMsgBankAccount123);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgBankAccount123 = "";
                    bankAccountMsg123.css('display', 'none');
                    finalOutput[0].Bank_Info3.Bank3_AccountNumber = $('#Vendor_Bank3_Account').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Bank_Holder":

                var bankHolderMsg1 = $("#Vendor_Bank_Holder_ErrorMsg");
                var errorMsgBankHolder = "";
                bankHolderMsg1.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Bank_Holder').val() === "" || $('#Vendor_Bank_Holder').val() === null) {
                    errorMsgBankHolder = "Please enter bank holder name";
                    bankHolderMsg1.append(errorMsgBankHolder);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgBankHolder = "";
                    bankHolderMsg1.css('display', 'none');
                    finalOutput[0].Bank_Info1.Bank1_AccountHolderName = $('#Vendor_Bank_Holder').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Bank2_Holder":

                var bankHolderMsg12 = $("#Vendor_Bank2_Holder_ErrorMsg");
                var errorMsgBankHolder12 = "";
                bankHolderMsg12.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Bank2_Holder').val() === "" || $('#Vendor_Bank2_Holder').val() === null) {
                    errorMsgBankHolder12 = "Please enter bank holder name";
                    bankHolderMsg12.append(errorMsgBankHolder12);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgBankHolder = "";
                    bankHolderMsg12.css('display', 'none');
                    finalOutput[0].Bank_Info2.Bank2_AccountHolderName = $('#Vendor_Bank2_Holder').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Bank3_Holder":

                var bankHolderMsg123 = $("#Vendor_Bank3_Holder_ErrorMsg");
                var errorMsgBankHolder123 = "";
                bankHolderMsg123.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Bank3_Holder').val() === "" || $('#Vendor_Bank3_Holder').val() === null) {
                    errorMsgBankHolder123 = "Please enter bank holder name";
                    bankHolderMsg123.append(errorMsgBankHolder123);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgBankHolder123 = "";
                    bankHolderMsg123.css('display', 'none');
                    finalOutput[0].Bank_Info3.Bank3_AccountHolderName = $('#Vendor_Bank3_Holder').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Bank_IBAN":

                var ibanMsg1 = $("#Vendor_Bank_IBAN_ErrorMsg");
                var errorMsgIban1 = "";
                ibanMsg1.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Bank_IBAN').val() === "" || $('#Vendor_Bank_IBAN').val() === null) {
                    errorMsgIban1 = "Please enter IBAN value";
                    ibanMsg1.append(errorMsgIban1);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgIban1 = "";
                    ibanMsg1.css('display', 'none');
                    finalOutput[0].Bank_Info1.Bank1_IbanValue = $('#Vendor_Bank_IBAN').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Bank2_IBAN":

                var ibanMsg12 = $("#Vendor_Bank2_IBAN_ErrorMsg");
                var errorMsgIban12 = "";
                ibanMsg12.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Bank2_IBAN').val() === "" || $('#Vendor_Bank2_IBAN').val() === null) {
                    errorMsgIban12 = "Please enter IBAN value";
                    ibanMsg12.append(errorMsgIban12);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgIban12 = "";
                    ibanMsg12.css('display', 'none');
                    finalOutput[0].Bank_Info2.Bank2_IbanValue = $('#Vendor_Bank2_IBAN').val();
                    e.preventDefault();
                }

                break;
            case "Vendor_Bank3_IBAN":

                var ibanMsg123 = $("#Vendor_Bank3_IBAN_ErrorMsg");
                var errorMsgIban123 = "";
                ibanMsg123.html('<div class="vendorError" style="display: block;"></div>');

                if ($('#Vendor_Bank3_IBAN').val() === "" || $('#Vendor_Bank3_IBAN').val() === null) {
                    errorMsgIban123 = "Please enter IBAN value";
                    ibanMsg123.append(errorMsgIban123);
                    e.preventDefault();
                    //return false;
                }
                else {
                    errorMsgIban123 = "";
                    ibanMsg123.css('display', 'none');
                    finalOutput[0].Bank_Info3.Bank3_IbanValue = $('#Vendor_Bank3_IBAN').val();
                    e.preventDefault();
                }

                break;
        }

    });

    $(document).on('change', '#Vendor_Bank_SwiftDDL,#Vendor_Bank2_SwiftDDL,#Vendor_Bank3_SwiftDDL', function () {
        var id = $(this)[0].id;

        if (id === 'Vendor_Bank_SwiftDDL')
            $('#Vendor_Bank_SwiftDDL_ErrorMsg').text('');
        else if (id === 'Vendor_Bank2_SwiftDDL')
            $('#Vendor_Bank2SwiftDDL_ErrorMsg').text('');
        else
            $('#Vendor_Bank3_SwiftDDL_ErrorMsg').text('');

    });

    ///////////////////////////////////////////////////  Tax Info Screen  //////////////////////////////////////////////////////////////////////////////

    function loadCurrency(currency, selectedCurrency, update = "") {
        var html_code = '';

        html_code += '<option value="" disabled selected>Select Currency</option>';

        $.each(currency, function (key, value) {

            if (value.order_currency !== "" || value.order_currency !== null) {
                if (update === "update" && selectedCurrency !== null && selectedCurrency !== "" && selectedCurrency === value.order_currency) {
                    html_code += '<option selected value="' + value.order_currency + '">' + value.data + '</option>';
                }
                else {
                    html_code += '<option value="' + value.order_currency + '">' + value.data + '</option>';
                }
            }

        });

        $("#Vendor_Tax_OrderDDL").html(html_code);
    }

    function loadJsonCurrency() {
        var html_code = '';

        $.getJSON('../Json/Currency.json', function (data) {

            html_code += '<option value="" disabled selected>Select Country</option>';

            $.each(data, function (key, value) {

                if (value.order_currency !== "" || value.order_currency !== null) {
                    html_code += '<option value="' + value.order_currency + '">' + value.data + '</option>';
                }

            });

            $("#Vendor_Tax_OrderDDL").html(html_code);
        });
    }

    function loadIncoTerms1(IncoTerms1, selectedIncoTerm1, update = "") {
        var html_code = '';

        html_code += '<option value="" disabled selected>Select Inco Terms1</option>';

        $.each(IncoTerms1, function (key, value) {

            if (value.data !== "" || value.data !== null) {

                if (update === "update" && selectedIncoTerm1 !== null && selectedIncoTerm1 !== "" && selectedIncoTerm1 === value.incoterms1) {
                    html_code += '<option selected value="' + value.incoterms1 + '">' + value.data + '</option>';
                }
                else {
                    html_code += '<option value="' + value.incoterms1 + '">' + value.data + '</option>';
                }
            }

        });

        $("#Vendor_Tax_Incoterms1DDL").html(html_code);
    }

    function loadJsonIncoTerms1() {
        var html_code = '';

        $.getJSON('../Json/IncoTerms1.json', function (data) {

            html_code += '<option value="" disabled selected>Select Inco Terms1</option>';

            $.each(data, function (key, value) {

                if (value.data !== "" || value.data !== null) {
                    html_code += '<option value="' + value.incoterms1 + '">' + value.data + '</option>';
                }

            });

            $("#Vendor_Tax_Incoterms1DDL").html(html_code);
        });
    }

    $('#TaxPreviousBtn').click(function () {
        $('#vendor_TaxInfo').removeClass('show active');
        $('#vendor_BankInfo').addClass('show active');
    });

    $('#Vendor_Tax_Taxnum1,#Vendor_Tax_Taxnum2,#Vendor_Tax_Taxnum3,#Vendor_Tax_Vat,#Vendor_Tax_Incoterms2,#Vendor_Tax_Delivery').focusout(function (e) {
        var id = $(this)[0].id;

        switch (id) {
            case "Vendor_Tax_Taxnum1":

                if ($('#Vendor_Tax_Taxnum1').val() !== null && $('#Vendor_Tax_Taxnum1').val() !== "")
                    finalOutput[0].Tax_Info.Tax_Number1 = $('#Vendor_Tax_Taxnum1').val();
                break;
            case "Vendor_Tax_Taxnum2":
                if ($('#Vendor_Tax_Taxnum2').val() !== null && $('#Vendor_Tax_Taxnum2').val() !== "")
                    finalOutput[0].Tax_Info.Tax_Number2 = $('#Vendor_Tax_Taxnum2').val();
                break;
            case "Vendor_Tax_Taxnum3":
                if ($('#Vendor_Tax_Taxnum3').val() !== null && $('#Vendor_Tax_Taxnum3').val() !== "")
                    finalOutput[0].Tax_Info.Tax_Number3 = $('#Vendor_Tax_Taxnum3').val();
                break;
            case "Vendor_Tax_Vat":
                if ($('#Vendor_Tax_Vat').val() !== null && $('#Vendor_Tax_Vat').val() !== "")
                    finalOutput[0].Tax_Info.Tax_VatRegNumber = $('#Vendor_Tax_Vat').val();
                break;
            case "Vendor_Tax_Incoterms2":

                var incoTerms = $('#Vendor_Tax_Incoterms2').val();

                var incoTerms2Msg = $("#Vendor_Tax_Incoterms2_ErrorMsg");
                var errorMsgIncoTerms2 = "";
                incoTerms2Msg.html('<div class="vendorError" style="display: block;"></div>');

                if (incoTerms.toLowerCase() === "place" || incoTerms.toLowerCase() === "port") {
                    $('#Vendor_Tax_Incoterms2_ErrorMsg').text('');
                    finalOutput[0].Tax_Info.Tax_IncoTerms2 = incoTerms;
                }
                else {
                    errorMsgIncoTerms2 = "Please Enter Place or Port";
                    incoTerms2Msg.append(errorMsgIncoTerms2);
                    e.preventDefault();
                }

                break;
            case "Vendor_Tax_Delivery":
                if ($('#Vendor_Tax_Delivery').val() !== null && $('#Vendor_Tax_Delivery').val() !== "")
                    finalOutput[0].Tax_Info.Tax_PlannedDeliveryDays = $('#Vendor_Tax_Delivery').val();
                break;

        }

    });

    $('#Vendor_Tax_Form').submit(function (e) {

        var taxCurrencyMsg = $("#Vendor_Tax_Order_ErrorMsg");
        var errorMsgtaxCurrency = "";
        taxCurrencyMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Tax_OrderDDL').val() === "" || $('#Vendor_Tax_OrderDDL').val() === null) {
            errorMsgtaxCurrency = "Please select currency";
            taxCurrencyMsg.append(errorMsgtaxCurrency);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgtaxCurrency = "";
            taxCurrencyMsg.css('display', 'none');
            finalOutput[0].Tax_Info.Tax_Currency = $('#Vendor_Tax_OrderDDL').val();
            e.preventDefault();
        }

        var incoTerms1Msg = $("#Vendor_Tax_Incoterms1DDL_ErrorMsg");
        var errorMsgIncoTerms1 = "";
        incoTerms1Msg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Tax_Incoterms1DDL').val() === "" || $('#Vendor_Tax_Incoterms1DDL').val() === null) {
            errorMsgIncoTerms1 = "Please select incoterms 1";
            incoTerms1Msg.append(errorMsgIncoTerms1);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgIncoTerms1 = "";
            incoTerms1Msg.css('display', 'none');
            finalOutput[0].Tax_Info.Tax_IncoTerms1 = $('#Vendor_Tax_Incoterms1DDL').val();
            e.preventDefault();
        }

        var incoTerms2Msg = $("#Vendor_Tax_Incoterms2_ErrorMsg");
        var errorMsgIncoTerms2 = "";
        incoTerms2Msg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Tax_Incoterms2').val() === "" || $('#Vendor_Tax_Incoterms2').val() === null) {
            errorMsgIncoTerms2 = "Please select incoterms 2";
            incoTerms2Msg.append(errorMsgIncoTerms2);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgIncoTerms2 = "";
            incoTerms2Msg.css('display', 'none');
            finalOutput[0].Tax_Info.Tax_IncoTerms2 = $('#Vendor_Tax_Incoterms2').val();
            e.preventDefault();
        }

        var taxDeliveryMsg = $("#Vendor_Tax_Delivery_ErrorMsg");
        var errorMsgTaxDelivery = "";
        taxDeliveryMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Tax_Delivery').val() === "" || $('#Vendor_Tax_Delivery').val() === null) {
            errorMsgTaxDelivery = "Please enter tax delivery time in days";
            taxDeliveryMsg.append(errorMsgTaxDelivery);
            e.preventDefault();
            //return false;
        }
        else {
            errorMsgTaxDelivery = "";
            taxDeliveryMsg.css('display', 'none');
            finalOutput[0].Tax_Info.Tax_PlannedDeliveryDays = $('#Vendor_Tax_Delivery').val();
            e.preventDefault();
        }

        console.log(finalOutput);

        if ($('#Vendor_Tax_OrderDDL').val() !== "" && $('#Vendor_Tax_OrderDDL').val() !== null && $('#Vendor_Tax_Incoterms1DDL').val() !== "" && $('#Vendor_Tax_Incoterms1DDL').val() !== null &&
            $('#Vendor_Tax_Incoterms2').val() !== "" && $('#Vendor_Tax_Incoterms2').val() !== null && $('#Vendor_Tax_Delivery').val() !== "" && $('#Vendor_Tax_Delivery').val() !== null) {

            //finalOutput[0].Company_Info.CompanyCode = Number.parseInt($('#Vendor_Profile_CompanyCodeDDL').val());
            //finalOutput[0].Company_Info.CompanyCode = $('#Vendor_Profile_PurchaseOrgDDL').val();

            $("#vendor_TaxInfo").removeClass('show active');
            $("#vendor_DocumentsInfo").addClass('show active');
            $("#vendor_Documents_Tab").addClass('active');
            //$("#vendor_Bank_Tab").css("pointer-events", "auto");
            $("#vendor_Documents_Tab").css("pointer-events", "auto");

            e.preventDefault();
        }

        event.preventDefault();
    });

    $(document).on('change', '#Vendor_Tax_OrderDDL,#Vendor_Tax_Incoterms1DDL', function () {
        var id = $(this)[0].id;

        if (id === 'Vendor_Tax_OrderDDL')
            $('#Vendor_Tax_Order_ErrorMsg').text('');
        else
            $('#Vendor_Tax_Incoterms1DDL_ErrorMsg').text('');

    });

    ///////////////////////////////////////////////////  Documents Info Screen  //////////////////////////////////////////////////////////////////////////////

    $('#Vendor_Doc_Article,#Vendor_Doc_Commercial,#Vendor_Doc_Statement,#Vendor_Doc_Authorised,#Vendor_Doc_Contract,#Vendor_Doc_Balance,#Vendor_Doc_Civil').on('change', function (e) {

        var numb = $(this)[0].files[0].size / 1024 / 1024; //count file size
        var resultid = $(this).val().split(".");
        var gettypeup = resultid[resultid.length - 1];
        var filesize = 5;
        numb = numb.toFixed(2);

        var title = "";
        var errorMsgtitle = "";
        fileId = $(this)[0].id;

        if (fileId === 'Vendor_Doc_Article') {
            title = $("#Vendor_Doc_Article_ErrorMsg");
        }

        if (fileId === 'Vendor_Doc_Commercial') {
            title = $("#Vendor_Doc_Commercial_ErrorMsg");
        }

        if (fileId === 'Vendor_Doc_Statement') {
            title = $("#Vendor_Doc_Statement_ErrorMsg");
        }

        if (fileId === 'Vendor_Doc_Authorised') {
            title = $("#Vendor_Doc_Authorised_ErrorMsg");
        }

        if (fileId === 'Vendor_Doc_Contract') {
            title = $("#Vendor_Doc_Contract_ErrorMsg");
        }

        if (fileId === 'Vendor_Doc_Balance') {
            title = $("#Vendor_Doc_Balance_ErrorMsg");
        }

        if (fileId === 'Vendor_Doc_Civil') {
            title = $("#Vendor_Doc_Civil_ErrorMsg");
        }

        title.html('<div class="vendorError" style="display: block;"></div>');

        if (gettypeup !== 'pdf') {
            $(this).val('');
            errorMsgtitle = "Please choose only PDF";
            title.append(errorMsgtitle);
            e.preventDefault();
            return false;
        }

        if (numb > filesize && gettypeup === 'pdf') {
            $(this).val('');
            errorMsgtitle = "Maximum upload file size is 5MB";
            title.append(errorMsgtitle);
            e.preventDefault();
            return false;
        }

    });

    $('#DocumentsPreviousBtn').click(function () {
        $('#vendor_DocumentsInfo').removeClass('show active');
        $('#vendor_TaxInfo').addClass('show active');
    });

    $('#Vendor_Document_Form').submit(function (e) {

        var commentMsg = $("#Vendor_Doc_Comment_ErrorMsg");
        var errorMsgcomment = "";
        commentMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#Vendor_Doc_Article').val() === "" || $('#Vendor_Doc_Article').val() === null || $('#Vendor_Doc_Commercial').val() === "" || $('#Vendor_Doc_Commercial').val() === null ||
            $('#Vendor_Doc_Statement').val() === "" || $('#Vendor_Doc_Statement').val() === null || $('#Vendor_Doc_Authorised').val() === "" || $('#Vendor_Doc_Authorised').val() === null ||
            $('#Vendor_Doc_Contract').val() === "" || $('#Vendor_Doc_Contract').val() === null || $('#Vendor_Doc_Balance').val() === "" || $('#Vendor_Doc_Balance').val() === null ||
            $('#Vendor_Doc_Civil').val() === "" || $('#Vendor_Doc_Civil').val() === null) {


            if ($('#Vendor_Doc_Comment').val() === "" || $('#Vendor_Doc_Comment').val() === null) {
                errorMsgcomment = "Please enter comment";
                commentMsg.append(errorMsgcomment);
                e.preventDefault();
            }
            else {
                Submit();
            }
        }
        else {
            console.log("please submit data");
        }

    });

    function Submit() {
        var fileUpload = $("#Vendor_Doc_Article").get(0);
        var files = fileUpload.files;

        // Create FormData object  
        var fileData = new FormData();

        // Looping over all files and add it to FormData object  
        for (var i = 0; i < files.length; i++) {
            fileData.append(files[i].name, files[i]);
        }

        $.ajax({
            url: '/VendorCreate/Submit',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            success: function (result) {
                alert(result);
            },
            error: function (err) {
                alert(err.statusText);
            }
        });
    }

    var codeCaptcha;
    var isCaptchaValid = false;

    function createCaptcha() {

        $('#captcha').html('');
        //document.getElementById('captcha').innerHTML = "";
        var charsArray =
            "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*";
        var lengthOtp = 6;
        var captcha = [];

        for (var i = 0; i < lengthOtp; i++) {
            //below code will not allow Repetition of Characters
            var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
            if (captcha.indexOf(charsArray[index]) === -1)
                captcha.push(charsArray[index]);
            else i--;
        }

        var canv = document.createElement("canvas");
        canv.id = "captcha";
        canv.width = 100;
        canv.height = 50;
        var ctx = canv.getContext("2d");
        ctx.font = "25px Georgia";
        ctx.strokeText(captcha.join(""), 0, 30);

        codeCaptcha = captcha.join("");
        //document.getElementById("captcha").appendChild(canv);
        $('#captcha').append(canv);
    }

    $('#cpatchaTextBox').focusout(function (e) {
        e.preventDefault();

        var captchaMsg = $("#Vendor_Doc_Captcha_ErrorMsg");
        var errorMsgCaptcha = "";
        captchaMsg.html('<div class="vendorError" style="display: block;"></div>');

        if ($('#cpatchaTextBox').val() === codeCaptcha) {
            $("#Vendor_Doc_Captcha_ErrorMsg").text('');
            isCaptchaValid = true;
        }
        else {
            errorMsgCaptcha = "Please Enter Valid Captcha";
            captchaMsg.append(errorMsgCaptcha);
            e.preventDefault();
            isCaptchaValid = false;
        }

    });


    ///////////////////////////////////////////////////  Documents Info Screen  //////////////////////////////////////////////////////////////////////////////

    //function getSample() {
    //    var sample = '';

    //    $.ajax({
    //        url: "http://localhost/RetailVendorRegService/api/values/TestSAPConnection",
    //        method: 'GET',
    //        crossDomain: true,
    //        headers: { 'Content-Type': 'application/json' },
    //        dataType: "json",
    //        success: function (response) {
    //            console.log(response); // server response
    //        },
    //        error: function (error) {
    //            console.log(error);
    //        }

    //    });
    //}

    //function getVendorDetails() {
    //    var vendorCode = {
    //        "VendorCode": "80038"
    //    };

    //    $.ajax({
    //        url: "http://localhost/RetailVendorRegService/api/values/GetVendorDetails",
    //        method: 'GET',
    //        crossDomain: true,
    //        headers: { 'Content-Type': 'application/json' },
    //        dataType: "json",
    //        data: vendorCode,
    //        success: function (response) {
    //            console.log(response); // server response
    //        },
    //        error: function (error) {
    //            console.log(error);
    //        }

    //    });
    //}

    //function getVendorDetailsByEmail() {
    //    var EmailID = "gowris@alghanim.com";

    //    //var email = {
    //    //    "EmailID": "" + "gowris@alghanim.com" +""
    //    //};

    //    $.ajax({
    //        url: "http://localhost/RetailVendorRegService/api/values/GetVendorDetailsByEmail?EmailID=" + '""' + EmailID + '""',
    //        method: 'GET',
    //        crossDomain: true,
    //        headers: { 'Content-Type': 'application/json' },
    //        dataType: "json",
    //        //data: email,
    //        success: function (response) {
    //            console.log(response); // server response
    //        },
    //        error: function (error) {
    //            console.log(error);
    //        }

    //    });
    //}

    //function EmailVerification() {

    //    var codes = {
    //        "VendorID": "80038",
    //        "CompanyCode": "1000"
    //    };

    //    $.ajax({
    //        url: "http://localhost/RetailVendorRegService/api/values/EmailVerification",
    //        method: 'GET',
    //        crossDomain: true,
    //        headers: { 'Content-Type': 'application/json' },
    //        dataType: "json",
    //        data: codes,
    //        success: function (response) {
    //            console.log(response); // server response
    //        },
    //        error: function (error) {
    //            console.log(error);
    //        }

    //    });

    //}

    $('#EmailID').focusout(function (e) {
        var emailID_Update = $('#EmailID').val();
        localStorage.setItem("emailID_Update", emailID_Update);
    });

    function GetJqueryVendor() {
        $.ajax({
            type: 'GET',
            //IIS Path
            url: RemoteURL + '/OTP/GetJqueryVendor',

            //Local
            //url: '/OTP/GetJqueryVendor',
            cache: false,
            success: function (result) {
                console.log(result);
                localStorage.setItem('vendorDetailsResult', result);
                setTimeout(integrateVendorDetails, 3000);
                $('#vendor_Home_tabs').removeClass('disabled');
                $('#vendor_Tabs_Content').removeClass('disabled');
                $('#vendor_Home_tabs').addClass('vendor_animate-bottom');
                $('#vendor_Tabs_Content').addClass('vendor_animate-bottom');
                //$('#vendor_Loader').css('display', 'none');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function integrateVendorDetails() {
        var result = localStorage.getItem('vendorDetailsResult');

        if (result !== null) {
            var vendorDetails = $.parseJSON(result);
            //var vendorDetails = JSON.parse(vendor);
            localStorage.setItem("vendorDetails", vendorDetails);
            update_Profile(vendorDetails);
            update_AddressInfo(vendorDetails);
            update_BankInfo(vendorDetails);
            update_TaxInfo(vendorDetails);
        }
    }

    function update_Profile(vendorDetails) {
        console.log(vendorDetails);
        var companyCode = vendorDetails.CompanyCode;
        //$('#Vendor_Profile_CompanyCodeDDL').val(companyCode);
        var listCodes = localStorage.getItem("AlfrescoCompanyCodes");
        var CompanyCodesList = $.parseJSON(listCodes);

        loadCompanyCodes(CompanyCodesList, Number.parseInt(companyCode), "update");

        var purchaseOrg = vendorDetails.PurchaseOrg;

        loadPurchaseOrganizations(companyCode, purchaseOrg, "update");
        //loadJsonPurchaseOrg(companyCode, purchaseOrg, "update");

        // $('#Vendor_Profile_PurchaseOrgDDL').val(purchaseOrg);

    }

    function update_AddressInfo(vendorDetails) {
        var VendorName = '';
        var title = vendorDetails.Title;

        //Local
        //title = "Mr.";

        $('#Vendor_Address_TitleDDL').val(title);

        VendorName = vendorDetails.VendorName1;
        if (vendorDetails.VendorName2 !== null && vendorDetails.VendorName2 !== undefined)
            VendorName += vendorDetails.VendorName2;
        if (vendorDetails.VendorName3 !== null && vendorDetails.VendorName3 !== undefined)
            VendorName += vendorDetails.VendorName3;

        $('#Vendor_Address_Name1').val(VendorName);

        $('#Vendor_Address_Line1').val(vendorDetails.AddressLine1);

        $('#Vendor_Address_Line2').val(vendorDetails.AddressLine2);

        $('#Vendor_Address_Line3').val(vendorDetails.AddressLine3);

        $('#Vendor_Address_Postal').val(vendorDetails.PostalCode);

        $('#Vendor_Address_City').val(vendorDetails.City);

        $('#Vendor_Address_CountryDDL').val(vendorDetails.Country);

        $('#Vendor_Address_Fax').val(vendorDetails.FaxNo);

        $('#Vendor_Address_Mobile').val(vendorDetails.MobileNo);

        $('#Vendor_Address_Telephone').val(vendorDetails.TelPhoneNo1);

        $('#Vendor_Address_Email').val(vendorDetails.EmailID);

    }

    function update_BankInfo(vendorDetails) {
        var list = localStorage.getItem('AlfrescoCountries');
        var countriesList = $.parseJSON(list);

        loadBankCountry(countriesList, vendorDetails.BankCountry, "update");

        $('#Vendor_Bank_CountryDDL').val(vendorDetails.BankCountry);

        $('#Vendor_Bank_NameDDL').val(vendorDetails.BankName);

        $('#Vendor_Bank_SwiftDDL').val(vendorDetails.BankKey);

        $('#Vendor_Bank_Account').val(vendorDetails.BankAccountNumber);

        $('#Vendor_Bank_Holder').val(vendorDetails.AccountHolderName);

        $('#Vendor_Bank_IBAN').val(vendorDetails.IBAN);
    }

    function update_TaxInfo(vendorDetails) {
        var list = localStorage.getItem("AlfrescoCurrency");
        var currencyList = $.parseJSON(list);

        loadCurrency(currencyList, vendorDetails.OrderCurrency, "update");

        var incoList = localStorage.getItem("AlfrescoIncoTerms1");
        var incoTerms1List = $.parseJSON(incoList);

        loadIncoTerms1(incoTerms1List, vendorDetails.IncoTerms1, "update");

        $('#Vendor_Tax_Taxnum1').val(vendorDetails.TaxNumber1);

        $('#Vendor_Tax_Taxnum2').val(vendorDetails.TaxNumber2);

        $('#Vendor_Tax_Taxnum3').val(vendorDetails.TaxNumber3);

        $('#Vendor_Tax_Vat').val(vendorDetails.VatRegNumber);

        //$('#Vendor_Tax_Delivery').val(vendorDetails.IBAN);

        //$('#Vendor_Tax_Incoterms1DDL').val(vendorDetails.IncoTerms1);

        $('#Vendor_Tax_Incoterms2').val(vendorDetails.IncoTerms2);

        $('#Vendor_Tax_Delivery').val(vendorDetails.PlannedDeliveryDays);
        $('#vendor_Loader').css('display', 'none');

    }

});