﻿using DotCMIS;
using DotCMIS.Client;
using DotCMIS.Client.Impl;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AlghanimVendor.Controllers
{
    public class VendorCreateController : Controller
    {
        public static StringBuilder log = new StringBuilder();

        // GET: VendorCreate
        public ActionResult Index()
        {
            try
            {
                log.Append("Index Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("Index Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "VendorCreate", "Index"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult Profile()
        {
            try
            {
                log.Append("Profile Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("Profile Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "VendorCreate", "Index"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult Address()
        {
            try
            {
                log.Append("Address Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("Address Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "VendorCreate", "Index"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult Bank()
        {
            try
            {
                log.Append("Bank Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("Bank Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "VendorCreate", "Index"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult Tax()
        {
            try
            {
                log.Append("Tax Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("Tax Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "VendorCreate", "Index"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult Documents()
        {
            try
            {
                log.Append("Documents Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("Documents Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "VendorCreate", "Index"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult WebReference()
        {
            return View();
        }

        public ActionResult Submit()
        {
            return RedirectToAction("WebReference");

            //if(Request.Files.Count > 0)
            //{
            //    try
            //    {
            //        HttpFileCollectionBase files = Request.Files;
            //        for (int i = 0; i < files.Count; i++)
            //        {
            //            string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";
            //            string filename = Path.GetFileName(Request.Files[i].FileName);

            //            HttpPostedFileBase file = files[i];
            //            string fname;
            //            if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
            //            {
            //                string[] testfiles = file.FileName.Split(new char[] { '\\' });
            //                fname = testfiles[testfiles.Length - 1];
            //            }
            //            else
            //            {
            //                fname = file.FileName;
            //            }

            //            fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
            //            file.SaveAs(fname);
            //        }

            //        return Json("File Uploaded Successfully!");
            //    }
            //    catch (Exception ex)
            //    {
            //        return Json("Error occurred. Error details: " + ex.Message);
            //    }
            //}
            //else
            //{
            //    return Json("No files selected.");
            //}
        }

        public void Authentication()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters[DotCMIS.SessionParameter.BindingType] = BindingType.AtomPub;

            parameters[DotCMIS.SessionParameter.AtomPubUrl] = "http://192.168.1.87:8080/alfresco/s/slingshot/node/content/workspace/SpacesStore/company_code?ALF_TICKET=<TICKET_f5708f323d8ed6fe2b20baef312312b7dce8a4cd>";
            parameters[DotCMIS.SessionParameter.User] = "admin";
            parameters[DotCMIS.SessionParameter.Password] = "admin";

            SessionFactory factory = SessionFactory.NewInstance();
            ISession session = factory.GetRepositories(parameters)[0].CreateSession();

            IOperationContext oc = session.CreateOperationContext();
            oc.IncludeAcls = true;

        }

        public static void LogToFile(StringBuilder message)
        {
            string logFolderName = "RetailVendorLogs";
            string fileName = "VendorLog";

            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + "\\" + logFolderName;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string filepath = path + "\\" + fileName + " - " + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
                if (!System.IO.File.Exists(filepath))
                {
                    using (StreamWriter sw = System.IO.File.CreateText(filepath))
                    {
                        sw.WriteLine(message);
                    }
                }
                else
                {
                    using (StreamWriter sw = System.IO.File.AppendText(filepath))
                    {
                        sw.WriteLine("\n" + message);
                    }
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Exception - WriteToFile() - " + ex.Message);
                log.Append("\nException - LogToFile() - " + ex.Message);
                throw;
            }
        }

    }
}