﻿using AlghanimVendor.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Net.Http.Headers;
using System.IO;

namespace AlghanimVendor.Controllers
{
    public class OTPController : Controller
    {
        private string Ticket;
        public static StringBuilder log = new StringBuilder();
        // GET: OTP
        public ActionResult GenerateOTP()
        {
            try
            {
                log.Append("GenerateOTP Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("GenerateOTP Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "OTP", "GenerateOTP"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        [HttpPost]
        public ActionResult GenerateOTP(OTPModel model)
        {
            try
            {
                TempData["OTPGenMail"] = model;
                Random rnd = new Random();
                int otp = rnd.Next(1000, 9999);
                string msg = otp.ToString();
                Session["OTP"] = otp;
                bool IsOTPSent = SendMail("Bharath@falconnect.in", model.EmailID, "OTP Verification", msg);
                if (IsOTPSent)
                {
                    log.Append("GenerateOTP Post OTP has been generated Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                    log.Append(Environment.NewLine);
                    Console.Write("otp sent successfully");
                }
                else
                {
                    log.Append("GenerateOTP Post OTP generating Failure. Time is " + DateTime.Now.ToString("hh:mm tt"));
                    log.Append(Environment.NewLine);
                    Console.Write("otp not sent");
                }
                return RedirectToAction("SubmitOTP");
            }
            catch (Exception ex)
            {
                log.Append("GenerateOTP Post Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "OTP", "GenerateOTP"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult SubmitOTP()
        {
            try
            {
                OTPModel model = (OTPModel)TempData["OTPGenMail"];

                log.Append("SubmitOTP has been Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View(model);
            }
            catch(Exception ex)
            {
                log.Append("SubmitOTP Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "OTP", "SubmitOTP"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        [HttpPost]
        public ActionResult SubmitOTP(OTPModel model)
        {
            try
            {
                int OTP = (int)Session["OTP"];

                if (ModelState.IsValid)
                {
                    if (OTP == model.OTPValue)
                    {
                        var update = "";
                        TempData["EmailID"] = model.EmailID;
                        //GetVendorDetails();

                        log.Append("Entered OTP is Valid OTP. Time is " + DateTime.Now.ToString("hh:mm tt"));
                        log.Append(Environment.NewLine);
                        return RedirectToAction("Index", "VendorCreate", new { update = "update" });
                    }
                    else
                    {
                        log.Append("Please Enter Valid OTP. Time is " + DateTime.Now.ToString("hh:mm tt"));
                        log.Append(Environment.NewLine);
                        ModelState.AddModelError("OTPValue", "Please Enter Valid OTP");
                        return View(model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                log.Append("SubmitOTP Post Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "OTP", "SubmitOTP"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public bool SendMail(string from, string to, string subject, string body)
        {
            bool IsOTPSent = false;
            //string bodyNew = "<main class='homeContainer'><h5 class='card-title'></h5><h4 class='card-subtitle mt - 5 mb - 4' style='color: #454545;text-align:inherit;'>OTP validation for vendor automation portal</h4><div class='card col-md-8'><div class='card-body'><div class='row justify-content-center mt-3 mb-3'><p class='text'>Hello vendor, <span class='txt-space'>" + body + "</span> is your OTP to authenticate your login. Don't share it with any one.</p></div></div></div></main>";
            //string bodyNew = "<main class='homeContainer'><h5 class='card-title'><img src='https://ci5.googleusercontent.com/proxy/2qkc0qbYim9o8gfG_mDzlg-IMuUaDhD1nVYKY8uWYwzglue1izgLKPAHyzuRJWe0hwb5uD5LORoDuBpU52a4SeTMlT7VoE7l5Vuh1YwRhzzfwVItxKTkofYRWC94ip4OIjNr3anb2ZCC6mm7CqMVFPSKi44=s0-d-e1-ft#http://tw-webserver7.teamworkpm.net/sites/alghanim1/images/A98A47AC910DA10DFDFD59D2D2D71DDA%2Egif' alt='Alghanim-Logo'></h5><h4 class='mt-5 mb-4' style='color:#454545;text-align:inherit; padding: 10px 15px 0;'>OTP validation for vendor automation portal</h4><div class='card col-md-8'><div style='-ms-flex: 1 1 auto; flex:1 1 auto;padding:1.25rem;'><div class='row justify-content-center mt-3 mb-3'><p class='text'>Hello vendor, <span class='txt-space'>" + body + "</span> is your OTP to authenticate your login. Don't share it with any one.</p></div></div></div></main>";

            //string bodyNew = "<main class='homeContainer'><h5 class='card-title'><a href = 'https://postimages.org/' target = '_blank' ><img src= 'https://i.postimg.cc/V5L3xKq1/Mail-icon.png' border = '0' alt = 'Mail-icon' /></a></h5><h4 class='mt-5 mb-4' style='color:#454545;text-align:inherit; padding: 10px 15px 0;font-size:medium;'>OTP validation for vendor automation portal</h4><div class='col-md-8' style='position: relative;display: -ms-flexbox;display: flex;-ms-flex-direction: column;flex-direction: column;min-width: 0;word-wrap: break-word;background-color: #fff;background-clip: border-box;border: 1px solid rgba(0, 0, 0, 0.125);border-radius: 0.25rem;'><div style='-ms-flex: 1 1 auto;flex: 1 1 auto;padding: 1.25rem;background-color:aliceblue;width:-webkit-fill-available;'><div class='row justify-content-center mt-3 mb-3'><p class='text' style='font-size:larger;'>Hello vendor,Your OTP From <a href='https://www.alghanim.com/'>Alghanim</a> is <span class='txt-space' style='font-size:larger;'>" + body + "</span>. Please Don't share it with any one.</p></div></div></div></main>";

            //string bodyNew = "<main class='homeContainer'><h5 class='card-title'><img src='https://ci5.googleusercontent.com/proxy/2qkc0qbYim9o8gfG_mDzlg-IMuUaDhD1nVYKY8uWYwzglue1izgLKPAHyzuRJWe0hwb5uD5LORoDuBpU52a4SeTMlT7VoE7l5Vuh1YwRhzzfwVItxKTkofYRWC94ip4OIjNr3anb2ZCC6mm7CqMVFPSKi44=s0-d-e1-ft#http://tw-webserver7.teamworkpm.net/sites/alghanim1/images/A98A47AC910DA10DFDFD59D2D2D71DDA%2Egif' alt='Alghanim-Logo'></h5><h4 class='mt-5 mb-4' style='color:#454545;text-align:inherit; padding: 10px 15px 0;font-size:medium;'>OTP validation for vendor portal</h4><div class='col-md-8' style='position: relative;display: -ms-flexbox;display: flex;-ms-flex-direction: column;flex-direction: column;min-width: 0;word-wrap: break-word;background-color: #fff;background-clip: border-box;border: 1px solid rgba(0, 0, 0, 0.125);border-radius: 0.25rem;'><div style='-ms-flex: 1 1 auto;flex: 1 1 auto;padding: 1.25rem;background-color:aliceblue;width:-webkit-fill-available;'><div class='row justify-content-center mt-3 mb-3'><p class='text' style='font-size:larger;'>Hello vendor,Your OTP From <a href='https://www.alghanim.com/'>Alghanim</a> is <span class='txt-space' style='font-size:larger;'>" + body + "</span>. Please Don't share it with any one.</p></div></div></div></main>";

            string bodyNew = "<div style='background-color:#6BBD0D !important;padding:0.5rem 5rem;'><h5 style='text-align:center; padding:1rem;'><img src='http://i66.tinypic.com/29czko.png' alt='Alghanim-Logo'></h5><div style='align-items:center; background-color: #fff; border-radius: 0.25rem;'><div style='align-items: center; padding: 2rem; box-shadow: 2px 6px 11px 1px #686868'><h5 style='text-align:center;'><img src='http://i65.tinypic.com/655ug7.png' alt='Alghanim-Logo'></h5><div style='text-align:center; padding:0.5rem;'><h3 style='color:#282828;'>Here is your One Time Password</h3><h5 style='color:#969696;'>to validate your email address</h5><h1 style='color:gray;'>" + body + "</h1><h5 style='color:red;'>Validate for 10 minutes only</h5></div></div></div><div style='color:white; text-align:center; padding: 1rem;'><p>This is a system generated mail. Please do not reply to this email ID.</p><p>If you have a query or need any clarification you may contact ITSD at <a href='javascript;' style='color:white;'>itservicedesk@alghanim.com</a></p></div></div>";


            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.To.Add(to);
                mailMessage.From = new MailAddress(from);
                mailMessage.Subject = subject;
                mailMessage.Body = bodyNew;
                mailMessage.IsBodyHtml = true;

                var smtp = new SmtpClient()
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(from, "barath123")
                };
                smtp.Send(mailMessage);

                log.Append("Mail Sent Successfyully");
                IsOTPSent = true;
            }
            catch (Exception ex)
            {
                IsOTPSent = false;
            }
            return IsOTPSent;
        }

        public async Task<string> GetVendorDetails()
        {
            string VendorCode = null; // = TempData["VendorCode"].ToString();
            string output = "";

            //Local
            VendorCode = "80038";

            //Remote
            //if (TempData["VendorCode"] != null)
            //    VendorCode = TempData["VendorCode"].ToString();

            try
            {
                string apiUrl = "http://localhost/RetailVendorRegService/api/values/GetVendorDetails?VendorCode=" + VendorCode;

                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        //var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);
                        output = JsonConvert.DeserializeObject(data).ToString();

                        Vendor vendor = JsonConvert.DeserializeObject<Vendor>(output);
                        //JObject jObject = JObject.Parse(output);
                        TempData["VendorDetails"] = vendor;
                        TempData.Keep();
                    }
                    else
                    {
                        output = "Data Failed"; 
                    }
                }
            }
            catch (Exception ex)
            {
                log.Append("GetVendorDetails from SAP is Failed." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                output = ex.Message;
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
            return output;
        }

        public async Task<string> GetVendorDetailsByEmail()
        {
            //Local
            string EmailID = "gowris@alghanim.com";

            //Remote
            //string EmailID = TempData["EmailID"].ToString();
            string output = "";

            try
            {
                string apiUrl = "http://localhost/RetailVendorRegService/api/values/GetVendorDetailsByEmail?EmailID=" + '"' + EmailID + '"';

                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        //var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);
                        output = JsonConvert.DeserializeObject(data).ToString();

                        JObject jObject = JObject.Parse(output);
                        TempData["VendorCode"] = Convert.ToInt32(jObject["vendorCode"]);
                    }
                    else
                    {
                        output = "Data Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                output = ex.Message;
            }

            return output;
        }

        public async Task<string> GetJqueryVendor()
        {
            var samp = await GetVendorDetails();
            Vendor vendor = (Vendor)TempData["VendorDetails"];
            var Json = JsonConvert.SerializeObject(vendor);
            return Json;
        }

        public ActionResult SampleTemplate()
        {
            return View();
        }

        public async Task<string> GetAlfrescoTicket()
        {
            string apiUrl = "http://192.168.1.87:8080/alfresco/s/api/login.json?u=admin&pw=admin";
            var data = "";

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    data = await response.Content.ReadAsStringAsync();
                    var obj = (JObject)JsonConvert.DeserializeObject(data);
                    Ticket = obj.Last.Last.Last.Last.ToString();
                    //var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

                }
            }

            return data;
        }

        public async Task<string> GetAlfrescoCompanyCodes()
        {
            //string Token = TempData["AlfrescoTicket"].ToString();

            string apiUrl = "http://192.168.1.87:8080/alfresco/s/slingshot/node/content/workspace/SpacesStore/company_code?ALF_TICKET=%3C" + Ticket +"%3E";
            var data = "";

            var credentials = new NetworkCredential("admin", "admin");
            var handler = new HttpClientHandler { Credentials = credentials };

            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    data = await response.Content.ReadAsStringAsync();
                    //var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

                }
            }

            return data;
        }

        public async Task<string> GetAlfrescoPurchaseOrg()
        {
            //string Token = TempData["AlfrescoTicket"].ToString();

            string apiUrl = "http://192.168.1.87:8080/alfresco/s/slingshot/node/content/workspace/SpacesStore/purchase_org?ALF_TICKET=%3C" + Ticket + "%3E";
            var data = "";

            var credentials = new NetworkCredential("admin", "admin");
            var handler = new HttpClientHandler { Credentials = credentials };

            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    data = await response.Content.ReadAsStringAsync();
                    //var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

                }
            }

            return data;
        }

        public async Task<string> GetAlfrescoCountries()
        {
            //string Token = TempData["AlfrescoTicket"].ToString();

            string apiUrl = "http://192.168.1.87:8080/alfresco/s/slingshot/node/content/workspace/SpacesStore/country_code?ALF_TICKET=%3C" + Ticket + "%3E";
            var data = "";

            var credentials = new NetworkCredential("admin", "admin");
            var handler = new HttpClientHandler { Credentials = credentials };

            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    data = await response.Content.ReadAsStringAsync();
                    //var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

                }
            }

            return data;
        }

        public async Task<string> GetAlfrescoTitles()
        {
            //string Token = TempData["AlfrescoTicket"].ToString();

            string apiUrl = "http://192.168.1.87:8080/alfresco/s/slingshot/node/content/workspace/SpacesStore/title?ALF_TICKET=%3C" + Ticket + "%3E";
            var data = "";

            var credentials = new NetworkCredential("admin", "admin");
            var handler = new HttpClientHandler { Credentials = credentials };

            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    data = await response.Content.ReadAsStringAsync();
                    //var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

                }
            }

            return data;
        }

        public async Task<string> GetAlfrescoCurrency()
        {
            //string Token = TempData["AlfrescoTicket"].ToString();

            string apiUrl = "http://192.168.1.87:8080/alfresco/s/slingshot/node/content/workspace/SpacesStore/order_currency?ALF_TICKET=%3C" + Ticket + "%3E";
            var data = "";

            var credentials = new NetworkCredential("admin", "admin");
            var handler = new HttpClientHandler { Credentials = credentials };

            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    data = await response.Content.ReadAsStringAsync();
                    //var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

                }
            }

            return data;
        }

        public async Task<string> GetAlfrescoIncoTerms1()
        {
            //string Token = TempData["AlfrescoTicket"].ToString();

            string apiUrl = "http://192.168.1.87:8080/alfresco/s/slingshot/node/content/workspace/SpacesStore/incoterms1?ALF_TICKET=%3C" + Ticket + "%3E";
            var data = "";

            var credentials = new NetworkCredential("admin", "admin");
            var handler = new HttpClientHandler { Credentials = credentials };

            using (HttpClient client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    data = await response.Content.ReadAsStringAsync();
                    //var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

                }
            }

            return data;
        }

        public static void LogToFile(StringBuilder message)
        {
            string logFolderName = "RetailVendorLogs";
            string fileName = "VendorLog";

            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + "\\" + logFolderName;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string filepath = path + "\\" + fileName + " - " + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
                if (!System.IO.File.Exists(filepath))
                {
                    using (StreamWriter sw = System.IO.File.CreateText(filepath))
                    {
                        sw.WriteLine(message);
                    }
                }
                else
                {
                    using (StreamWriter sw = System.IO.File.AppendText(filepath))
                    {
                        sw.WriteLine("\n" + message);
                    }
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Exception - WriteToFile() - " + ex.Message);
                log.Append("\nException - LogToFile() - " + ex.Message);
                throw;
            }
        }

    }
}