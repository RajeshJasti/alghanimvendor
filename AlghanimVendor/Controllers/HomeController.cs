﻿using System;
using System.IO;
using System.Text;
using System.Web.Mvc;

namespace CompShop.Controllers
{
    public class HomeController : Controller
    {
        public static StringBuilder log = new StringBuilder();

        public ActionResult Index()
        {
            try
            {
                log.Append("Index Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("Index Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "Home", "Index"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            try
            {
                log.Append("Index Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("Index Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "Home", "Index"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult SelectBU()
        {
            ViewBag.Message = "Your application description page.";

            try
            {
                log.Append("SelectBU Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("SelectBU Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "Home", "Index"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult SelectVendor()
        {
            ViewBag.Message = "Your contact page.";

            try
            {
                log.Append("SelectVendor Page Loaded Successfully. Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View();
            }
            catch (Exception ex)
            {
                log.Append("SelectVendor Page Loading Failure." + ex.Message + ". Time is " + DateTime.Now.ToString("hh:mm tt"));
                log.Append(Environment.NewLine);
                return View("Error", new HandleErrorInfo(ex, "Home", "Index"));
            }
            finally
            {
                LogToFile(log);
                log.Clear();
            }
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult Enquiry()
        {
            return View();
        }

        public ActionResult Purchase()
        {
            return View();
        }

        public ActionResult DarkLayoutPage()
        {
            return View();
        }

        public static void LogToFile(StringBuilder message)
        {
            string logFolderName = "RetailVendorLogs";
            string fileName = "VendorLog";

            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + "\\" + logFolderName;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string filepath = path + "\\" + fileName + " - " + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
                if (!System.IO.File.Exists(filepath))
                {
                    using (StreamWriter sw = System.IO.File.CreateText(filepath))
                    {
                        sw.WriteLine(message);
                    }
                }
                else
                {
                    using (StreamWriter sw = System.IO.File.AppendText(filepath))
                    {
                        sw.WriteLine("\n" + message);
                    }
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Exception - WriteToFile() - " + ex.Message);
                log.Append("\nException - LogToFile() - " + ex.Message);
                throw;
            }
        }

    }
}