﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlghanimVendor.Models
{
    public class Vendor
    {
        public string AccountGroup { get; set; }

        public string Title { get; set; }

        public string VendorName1 { get; set; }

        public string VendorName2 { get; set; }

        public string VendorName3 { get; set; }

        public string VendorName4 { get; set; }

        public string SearchItem1 { get; set; }

        public string SearchItem2 { get; set; }

        public string Country { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string Language { get; set; }

        public string TelNoCountryCode { get; set; }

        public string TelPhoneNo1 { get; set; }

        public string MobileNoCountryCode { get; set; }

        public string MobileNo { get; set; }

        public string FaxNoCountryCode { get; set; }

        public string FaxNo { get; set; }

        public string EmailID { get; set; }

        public string BankCountry { get; set; }

        public string BankKey { get; set; }

        public string BankAccountNumber { get; set; }

        public string AccountHolderName { get; set; }

        public string IBAN { get; set; }

        public string IBANValidFrom { get; set; }

        public string BankName { get; set; }

        public string BankAddress { get; set; }

        public string InstructionKey { get; set; }

        public string VatRegNumber { get; set; }

        public string TaxNumber1 { get; set; }

        public string TaxNumber2 { get; set; }

        public string TaxNumber3 { get; set; }

        public string Reference { get; set; }

        public string Reconcilation { get; set; }

        public string CompanyCode { get; set; }

        public string PaymentTerms { get; set; }

        public string CheckDoubleINV { get; set; }

        public string PaymentMethods { get; set; }

        public string PaymentBLock { get; set; }

        public string OrderCurrency { get; set; }

        public string IncoTerms1 { get; set; }

        public string IncoTerms2 { get; set; }

        public string SchemaGroup { get; set; }

        public string GRBasedINV { get; set; }

        public string PurchaseGroup { get; set; }

        public string PurchaseOrg { get; set; }

        public string PlannedDeliveryDays { get; set; }

        public string ConfirmationControl { get; set; }

        public string ShippingCondition { get; set; }
    }
}