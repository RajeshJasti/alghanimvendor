﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AlghanimVendor.Models
{
    public class OTPModel
    {
        [Display(Name = "Email ID")]
        [Required(ErrorMessage = "Enter Email ID")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string EmailID { get; set; }

        [Display(Name ="Enter OTP")]
        //[Range(1,10000,ErrorMessage ="Max 4 Values are Allowed")]
        [Required(ErrorMessage = "Enter OTP Value")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Enter only Numbers")]
        public int OTPValue { get; set; }

    }
}