﻿var companyNextBtnClicked = 0;
var bankNextBtnClicked = 0;
var addressNextBtnClicked = 0;

var baseURL = "http://localhost:9057/VendorCreate/";

$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: baseURL + "GetCompanyCode",
        success: function (data) {
            var obj = JSON.parse(data);
            $('#ddlCompanyCode').append('<option value="--Select--">--Select--</option>');
            $('#ddlPurchaseOrganization').append('<option value="--No Data Available--">--No Data Available--</option>');
            $.each(obj, function (i, item) {
                $('#ddlCompanyCode').append('<option value="' + item.value + '">' + item.text + '</option>');
            });
        },

        failure: function (data) {
            alert('Failure - ' + data.responseText);
        },

        error: function (data) {
            alert('Error - ' + data.status + ' : ' + data.errorMessage);
        }
    });

    //$("input").keyup(function () {
    //    $(this).val($(this).val().toUpperCase());
    //});

});

$.LoadDropdown = function (methodName, controlName) {
    $.ajax({
        type: "GET",
        url: baseURL + methodName,
        success: function (data) {
            var obj = JSON.parse(data);
            $('#' + controlName).append('<option value="--Select--">--Select--</option>');
            $.each(obj, function (i, item) {
                $('#' + controlName).append('<option value="' + item.value + '">' + item.text + '</option>');
            });
        },

        failure: function (data) {
            alert('Failure - ' + data.responseText);
        },

        error: function (data) {
            alert('Error - ' + data.status + ' : ' + data.errorMessage);
        }
    });
};

$("#txtPostalCode1").keypress(function () {
    $.Numeric(event);
});

$("#txtFaxNumber1").keypress(function () {
    $.Numeric(event);
});

$("#txtMobileNumber1").keypress(function () {
    $.Numeric(event);
});

$("#txtTelephoneNumber1").keypress(function () {
    $.Numeric(event);
});

$("#txtPostalCode2").keypress(function () {
    $.Numeric(event);
});

$("#txtFaxNumber2").keypress(function () {
    $.Numeric(event);
});

$("#txtMobileNumber2").keypress(function () {
    $.Numeric(event);
});

$("#txtTelephoneNumber2").keypress(function () {
    $.Numeric(event);
});

$.Numeric = function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
};

$('#ddlCompanyCode').change(function () {
    var companyCodeValue = $("#ddlCompanyCode option:selected").val();
    if (companyCodeValue != '--Select--') {
        $('#ddlPurchaseOrganization').empty();
        $.ajax({
            type: "GET",
            url: baseURL + "GetPurchaseOrganization",
            data: { companyCode: companyCodeValue },
            success: function (data) {
                var obj = JSON.parse(data);
                $('#ddlPurchaseOrganization').append('<option value="--Select--">--Select--</option>');
                $.each(obj, function (i, item) {
                    $('#ddlPurchaseOrganization').append('<option value="' + item.value + '">' + item.text + '</option>');
                });
            },

            failure: function (data) {
                alert('Failure - ' + data.responseText);
            },

            error: function (data) {
                alert('Error - ' + data.status + ' : ' + data.errorMessage);
            }
        });
    }
    else {
        $('#ddlPurchaseOrganization').empty();
        $('#ddlPurchaseOrganization').append('<option value="--No Data Available--">--No Data Available--</option>');
    }
});

$('#companyNextBtn').click(function () {

    var ddlCompanyCode = $("#ddlCompanyCode option:selected").val();
    var ddlpurchaseOrganization = $("#ddlPurchaseOrganization option:selected").val();

    if (ddlCompanyCode == "--Select--") {
        $('#ddlCompanyCode').focus();
        alert('Select Company Code');
        return;
    }

    if (ddlpurchaseOrganization == "--Select--") {
        $('#ddlpurchaseOrganization').focus();
        alert('Select Purchase Organization');
        return;
    }

    companyNextBtnClicked++;

    $('#company-just').removeClass('show active');
    $('#company-tab-just').removeClass('active');
    $('#company-tab-just').attr('aria-selected', 'false');

    $('#addressTab').removeClass('disableTab');
    $('#address-just').addClass('show active');
    $('#address-tab-just').addClass('active');
    $('#address-tab-just').attr('aria-selected', 'true');

    if (companyNextBtnClicked == 1) {
        $.LoadDropdown('GetTitle', 'ddlTitle1');
        $.LoadDropdown('GetTitle', 'ddlTitle2');
        $.LoadDropdown('GetCountry', 'ddlCountry1');
        $.LoadDropdown('GetCountry', 'ddlCountry2');
    }
});

$('#addressBackBtn').click(function () {
    $('#address-tab-just').removeClass('active');
    $('#address-just').removeClass('show active');
    $('#address-tab-just').attr('aria-selected', 'false');

    $('#companyTab').removeClass('disableTab');
    $('#company-tab-just').addClass('active');
    $('#company-just').addClass('show active');
    $('#company-tab-just').attr('aria-selected', 'true');
});

$('#addressNextBtn').click(function () {
    var ddltitle = $("#ddlTitle1 option:selected").val();
    var txtName1 = $('#txtName1_1').val();
    var txtAddressLine1_1 = $('#txtAddressLine1_1').val();
    var txtPostalCode1 = $('#txtPostalCode1').val();
    var txtCity1 = $('#txtCity1').val();
    var ddlCountry1 = $("#ddlCountry1 option:selected").val();
    var txtMobileNumber1 = $('#txtMobileNumber1').val();
    var txtEmailID1 = $('#txtEmailID1').val();

    if (ddltitle == "--Select--") {
        alert('Select Title');
        $('#ddltitle').focus();
        return;
    }

    if (txtName1 == "") {
        alert('Enter Name 1');
        $('#txtName1').focus();
        return;
    }

    if (txtAddressLine1_1 == "") {
        alert('Enter Address Line 1');
        $('#txtAddressLine1_1').focus();
        return;
    }

    if (txtPostalCode1 == "") {
        alert('Enter Postal Code');
        $('#txtPostalCode1').focus();
        return;
    }

    if (txtCity1 == "") {
        alert('Enter City');
        $('#txtCity1').focus();
        return;
    }

    if (ddlCountry1 == "--Select--") {
        alert('Select Country');
        $('#ddlCountry1').focus();
        return;
    }

    if (txtMobileNumber1 == "") {
        alert('Enter Mobile Number');
        $('#txtMobileNumber1').focus();
        return;
    }

    if (txtEmailID1 == "") {
        alert('Enter Email ID');
        $('#txtEmailID1').focus();
        return;
    }

    addressNextBtnClicked++;

    $('#address-tab-just').removeClass('active');
    $('#address-just').removeClass('show active');
    $('#address-tab-just').attr('aria-selected', 'false');

    $('#bankTab').removeClass('disableTab');
    $('#bank-tab-just').addClass('active');
    $('#bank-just').addClass('show active');
    $('#bank-tab-just').attr('aria-selected', 'true');

    if (addressNextBtnClicked == 1) {
        $.LoadDropdown('GetCountry', 'ddlBankCountry1');
        $.LoadDropdown('GetCountry', 'ddlBankCountry2');
        $.LoadDropdown('GetBankKey', 'ddlBankKey1');
        $.LoadDropdown('GetBankKey', 'ddlBankKey2');
        $.LoadDropdown('GetBankName', 'ddlBankName1');
        $.LoadDropdown('GetBankName', 'ddlBankName2');
    }
});

$('#bankBackBtn').click(function () {
    $('#bank-tab-just').removeClass('active');
    $('#bank-just').removeClass('show active');
    $('#bank-tab-just').attr('aria-selected', 'false');

    $('#addressTab').removeClass('disableTab');
    $('#address-tab-just').addClass('active');
    $('#address-just').addClass('show active');
    $('#address-tab-just').attr('aria-selected', 'true');
});

$('#bankNextBtn').click(function () {
    var ddlBankCountry1 = $("#ddlBankCountry1 option:selected").val();
    var ddlBankKey1 = $("#ddlBankKey1 option:selected").val();
    var ddlBankName1 = $("#ddlBankName1 option:selected").val();
    var txtBankAccount1 = $('#txtBankAccount1').val();
    var txtAccountHolder1 = $('#txtAccountHolder1').val();
    var txtIbanValue1 = $('#txtIbanValue1').val();

    if (ddlBankCountry1 == "--Select--") {
        alert('Select Country');
        $('#ddlBankCountry1').focus();
        return;
    }

    if (ddlBankKey1 == "--Select--") {
        alert('Select Bank Key / Swift Code');
        $('#ddlBankKey1').focus();
        return;
    }

    if (ddlBankName1 == "--Select--") {
        alert('Select Bank Name');
        $('#ddlBankName1').focus();
        return;
    }

    if (txtBankAccount1 == "") {
        alert('Enter Bank Account');
        $('#txtBankAccount1').focus();
        return;
    }

    if (txtAccountHolder1 == "") {
        alert('Enter Account Holder');
        $('#txtAccountHolder1').focus();
        return;
    }

    if (txtIbanValue1 == "") {
        alert('Enter IBAN Value');
        $('#txtIbanValue1').focus();
        return;
    }

    bankNextBtnClicked++;

    $('#bank-tab-just').removeClass('active');
    $('#bank-just').removeClass('show active');
    $('#bank-tab-just').attr('aria-selected', 'false');

    $('#taxTab').removeClass('disableTab');
    $('#tax-tab-just').addClass('active');
    $('#tax-just').addClass('show active');
    $('#tax-tab-just').attr('aria-selected', 'true');

    if (bankNextBtnClicked == 1) {
        $.LoadDropdown('GetOrderCurrency', 'ddlOrderCurrency');
        $.LoadDropdown('GetIncoterms1', 'ddlIncoterms1');
    }
});

$('#taxBackBtn').click(function () {
    $('#tax-tab-just').removeClass('active');
    $('#tax-just').removeClass('show active');
    $('#tax-tab-just').attr('aria-selected', 'false');

    $('#bankTab').removeClass('disableTab');
    $('#bank-tab-just').addClass('active');
    $('#bank-just').addClass('show active');
    $('#bank-tab-just').attr('aria-selected', 'true');
});

$('#taxNextBtn').click(function () {
    var ddlOrderCurrency = $("#ddlOrderCurrency option:selected").val();
    var ddlIncoterms1 = $("#ddlIncoterms1 option:selected").val();
    var ddlIncoterms2 = $("#ddlIncoterms2 option:selected").val();
    var txtPlannedDeliveryTime = $('#txtPlannedDeliveryTime').val();

    if (ddlOrderCurrency == "--Select--") {
        alert('Select Order Currency');
        $('#ddlOrderCurrency').focus();
        return;
    }

    if (ddlIncoterms1 == "--Select--") {
        alert('Select Incoterms 1');
        $('#ddlIncoterms1').focus();
        return;
    }

    if (ddlIncoterms2 == "--Select--") {
        alert('Select Incoterms 2');
        $('#ddlIncoterms2').focus();
        return;
    }

    if (txtPlannedDeliveryTime == "") {
        alert('Enter Planned Delivery Time');
        $('#txtPlannedDeliveryTime').focus();
        return;
    }

    $('#tax-tab-just').removeClass('active');
    $('#tax-just').removeClass('show active');
    $('#tax-tab-just').attr('aria-selected', 'false');

    $('#documentsTab').removeClass('disableTab');
    $('#documents-tab-just').addClass('active');
    $('#documents-just').addClass('show active');
    $('#documents-tab-just').attr('aria-selected', 'true');
});

$('#documentsBackBtn').click(function () {
    $('#documents-tab-just').removeClass('active');
    $('#documents-just').removeClass('show active');
    $('#documents-tab-just').attr('aria-selected', 'false');

    $('#taxTab').removeClass('disableTab');
    $('#tax-tab-just').addClass('active');
    $('#tax-just').addClass('show active');
    $('#tax-tab-just').attr('aria-selected', 'true');
});

